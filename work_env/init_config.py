#!/usr/bin/env python

import os
import sys
import optparse
import errno

# Const set ----------------------------------------

CURRENT_DIR = os.path.normpath(os.getcwd())
SETTINGS_DIR = os.path.normpath(os.path.join(CURRENT_DIR, "settings"))
SRC_DIR = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

VAR_FILE= os.path.normpath(os.path.join(CURRENT_DIR, "build_variables.py"))

#/*------------- Start of BuildManager ------------#

class BuildManager:
  def __init__(self, options):
    self.options = options
    self.init_from_build_variables_file();

  def init_from_build_variables_file(self):
    if self.options.init:
      print("[ Init ]")
      if not os.path.isfile(VAR_FILE):
        print("Copying from %s.in ..." % (VAR_FILE))
        self.sh("cp -f", VAR_FILE + ".in", VAR_FILE)
      else:
        print("Warning: build_variables.py is already exist.")
    elif not os.path.isfile(VAR_FILE):
      print("Error: Can't find %s." % (VAR_FILE))
      print("  Require %s.\n  It should be copied from %s.in." % (VAR_FILE, VAR_FILE))
      print("\n  Run `./init_config.py --init` for copy.\n")
      sys.exit(-1)

    import build_variables
    self.git_install_pkg = build_variables.GIT_INSTALL_PKG
    self.git_init = build_variables.GIT_INIT
    self.set_mkscope = build_variables.SET_MKSCOPE


  def all(self):
    print("[ All ]")
    self.initial_settings()
    self.configure()

  def initial_settings(self):
    print("[ Initialize Settings ]")
    if self.git_install_pkg is True:
      print("Git update")
      self.sh(os.path.join(SETTINGS_DIR, "git_install_packages.sh"))
    if self.git_init is True:
      print("Git init")
      self.sh(os.path.join(SETTINGS_DIR, "git_init.sh"))
    if self.set_mkcscope is True:
      print("Set mkcscope.sh to /usr/bin/mkcscope.sh")
      self.sh(os.path.join(SETTINGS_DIR, "git_init.sh"))
      self.sh("cp -f", os.path.join(SETTINGS_DIR, "mkcscope.sh"), "/usr/bin/");

  def configure(self):
    print("[ Configure ]")

# Utils --------------------------------------------
  # Shell Command Without Fail
  def shWOF(self, *commands):
    command = ' '.join(commands)
    return os.system(command)

  # Shell Command
  def sh(self, *commands):
    ret = self.shWOF(*commands)
    if ret:
      sys.exit(1)

# CommandLine args ---------------------------------
  @staticmethod
  def parse_args(parser, argv):
    parser.add_option('--init', action='store_true',
                      help='Create build_variables.py from .in file.')
    parser.add_option('--all', action='store_true',
                      help='Execute all options.')
    parser.add_option('-i', '--initial-settings', action='store_true',
                      help='Set default environment.')
    parser.add_option('-c', '--configure', action='store_true',
                      help='Configure ToGate : execute gn ')

    parser.add_option('-t', '--target', type='string', default='chrome',
                      help='Specify target for build. |togate| is default target')

    return parser.parse_args(argv)

#-------------- End of BuildManager -------------*/#


# Main for init_config.py --------------------------
def main(argv):
  parser = optparse.OptionParser(description=sys.modules[__name__].__doc__)
  options, args = BuildManager.parse_args(parser, argv)

  if not argv:
    print("Error: Require one or more options.\n")
    parser.print_help()
    return errno.EINVAL
  if args:
    parser.error('Unrecognized command line arguments: %s.' % ', '.join(args))
    return errno.EINVAL

  bm = BuildManager(options)
  if options.all:
    bm.all()
  else:
    if options.initial_settings:
      bm.initial_settings()
    if options.configure:
      bm.configure()

  print("Success!")

if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
