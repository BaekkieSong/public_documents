# Locale 설정
## Ubuntu(WSL) 한글 설정
* 한글팩 설치
  * apt install
    * apt-get install language-pack-ko
    * apt-get install language-pack-ko-base
  * Locale 선택
    * apt-get install localepurge
      * GUI에서 ko_KR.EUC-KR, ko_KR.UTF-8 등을 선택
  * 한글팩 재적용
    * locale-gen --purge
    * dpkg-reconfigure locales
## 한글 깨짐 이슈 해결 방법
* 1. LANG 설정
  * shell에서 `locale`을 입력해 LANG 설정 정보 확인
  * 지원 언어 추가
    * vim /var/lib/locales/supported.d/ko
    * `ko_KR.EUC-KR EUC-KR` 추가
  * 한글 지원 환경 추가
    * vim /etc/environment
      * `LANG="ko_KR.UTF-8"` 추가
      * `LANG="ko_KR.EUC-KR"` 추가
  * reboot하여 설정 확인
* 2. 글꼴 변경
  * 특정 글꼴로 설정시 한글이 깨지는 이슈가 있음
  * 속성 -> 글꼴에서 다른 기본 글꼴(Gothic)으로 변경
---
### 출처
* https://seulkom.tistory.com/100
* https://chanhy63.tistory.com/9
