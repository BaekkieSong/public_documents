# 환경 설정
## 전제조건(precondition)
* git설치
* python 설치 및 환경설정(export)
  * export 설정 방법
    * `which python` 으로 python 경로 확인 ex) /usr/bin/python
    * `export PATH=$PATH:/usr/bin/python` 으로 경로 설정
## 사용법
* `./init_config.py --init`
* `vim ./build_variables.py` 후, 원하는 옵션 설정
* `./init_config.py --all`
### init_config.py 옵션 설명
* `--init`: 초기 옵션 설정
  * build_variables.py.in -> build_variables.py 복사
* `--all`: --init을 제외한 모든 옵션 동작 실행
* `-i`: 초기화 작업
  * `./settings/git_install_packages.sh` 실행
  * `./settings/git_init.sh` 실행
* `-c`: 환경 구성 작업

