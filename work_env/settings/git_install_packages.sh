# apt server path - /etc/apt/source.list
sudo apt update
sudo apt upgrade
sudo apt autoremove

# install packages
sudo apt install cmake clang tmux language-pack-ko tree
sudo locale-gen ko_KR.UTF-8
sudo dpkg-reconfigure locales

