# Global User 정보 등록
git config --global user.email "BaekkieSong@github.com"
git config --global user.name "BaekkieSong"
# `git pull` 할 때, fast forward인 경우에만 허용 + 원격repo를 base로 재설정
git config pull.ff only
git config pull.rebase true
# 수정사항을 `git add *` 할 때, 스테이징 대상에서 .gitignore에 해당되는 파일 제외
git config advice.addIgnoredFile false

