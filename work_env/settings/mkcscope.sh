#!/bin/sh
rm -rf cscope.files cscope.out
rm -rf tags
#cscope.files 생성
find . \( -name '*.c' -o -name '*.cpp' -o -name '*.cc' -o -name '*.h' -o -name '*.s' -o -name '*.S' -o -name '*.asm' \) -print > cscope.files
#tags 생성
ctags -R
#cscope.out 생성
cscope -i cscope.files
