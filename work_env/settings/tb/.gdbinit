define col
    set $cols= (listc_t *)&($arg0->rel->cols)
    set $_pos=(listc_t *)($cols)->link.next
    while ($_pos != ($cols))
        print $_pos->data
        print *(expn_t *)$_pos->data
        set $_pos = (listc_t *)$_pos->link.next
    end
end

define lpn2
set $lpn = (lpn_t *)$arg0
    if $lpn->type == LPN_TBL
        print *(lpn_tbl_t*)$lpn
    end
    if $lpn->type == LPN_JOIN
        print *(lpn_join_t*)$lpn
    end
    if $lpn->type == LPN_SET 
        print *(lpn_set_t*)$lpn
    end
    if $lpn->type == LPN_ORDERBY
        print *(lpn_orderby_t*)$lpn
    end
    if $lpn->type == LPN_PROJ
        print *(lpn_proj_t*)$lpn
    end
    if $lpn->type == LPN_GRP
        print *(lpn_grp_t*)$lpn
    end
    if $lpn->type == LPN_SEL
        print *(lpn_sel_t*)$lpn
    end
    if $lpn->type == LPN_CONNBY
        print *(lpn_connby_t*)$lpn
    end
    if $lpn->type == LPN_INSERT
        print *(lpn_insert_t*)$lpn
    end
    if $lpn->type == LPN_UPDATE
        print *(lpn_update_t*)$lpn
    end
    if $lpn->type == LPN_DELETE
        print *(lpn_delete_t*)$lpn
    end
    if $lpn->type == LPN_MERGE
        print *(lpn_merge_t*)$lpn
    end
    if $lpn->type == LPN_CALL
        print *(lpn_call_t*)$lpn
    end
    if $lpn->type == LPN_ANALYTIC
        print *(lpn_analytic_t*)$lpn
    end
    if $lpn->type == LPN_APPLY
        print *(lpn_apply_t*)$lpn
    end
    if $lpn->type == LPN_PROXY
        print *(lpn_proxy_t*)$lpn
    end
    if $lpn->type == LPN_BUFF
        print *(lpn_buff_t*)$lpn
    end
    if $lpn->type == LPN_MTBL_INSERT
        print *(lpn_mtbl_insert_t*)$lpn
    end
    if $lpn->type == LPN_INSTOF
        print *(lpn_instof_t*)$lpn
    end
    if $lpn->type == LPN_XML
        print *(lpn_xml_t*)$lpn
    end
    if $lpn->type == LPN_UNPIVOT
        print *(lpn_unpivot_t*)$lpn
    end
end

define lpn
set $lpn = (lpn_t*)(&($arg0))
    lpn2 $lpn
end

define lchild
set $lpn = (lpn_t*)(&($arg0))
set $child = lpn__get_child($lpn)
		lpn2 $child
end

define lchild2
set $lpn = (lpn_t*)$arg0
set $child = lpn__get_child($lpn)
		lpn2 $child
end

define path2
set $path = (path_t*)($arg0)
    if $path->type == OP_DPV
        print *(path_tscan_t*)$path
    end
    if $path->type == OP_TSCAN_ROWID 
        print *(path_ridscan_t*)$path
    end
    if $path->type == OP_TSCAN_FULL
        print *(path_tscan_t*)$path
    end
    if $path->type == OP_JOIN_HASH
        print *(path_join_t*)$path
    end
    if $path->type == OP_JOIN_MERGE
        print *(path_join_t*)$path
    end
    if $path->type == OP_JOIN_NESTED  
        print *(path_join_t*)$path
    end
    if $path->type == OP_JOIN_IDX_NESTED
        print *(path_join_t*)$path
    end
    if $path->type == OP_SET_UNION 
        print *(path_set_t*)$path
    end
    if $path->type == OP_SET_INTERSECT
        print *(path_set_t*)$path
    end
    if $path->type == OP_SET_MINUS
        print *(path_set_t*)$path
    end
    if $path->type == OP_BAG_UNION 
        print *(path_set_t*)$path
    end
    if $path->type == OP_PROJECT 
        print *(path_proj_t*)$path
    end
    if $path->type == OP_FILTER 
        print *(path_sel_t*)$path
    end
    if $path->type == OP_COUNT_ROWNUM 
        print *(path_sel_t*)$path
    end
    if $path->type == OP_ORDERBY 
        print *(path_sh_t*)$path
    end
    if $path->type == OP_GROUPBY_SORT
        print *(path_sh_t*)$path
    end
    if $path->type == OP_DISTINCT_SORT
        print *(path_sh_t*)$path
    end
    if $path->type == OP_GROUPBY
        print *(path_sh_t*)$path
    end
    if $path->type == OP_DISTINCT
        print *(path_sh_t*)$path
    end
    if $path->type == OP_GROUPBY_HASH
        print *(path_sh_t*)$path
    end
    if $path->type == OP_DISTINCT_HASH
        print *(path_sh_t*)$path
    end
    if $path->type == OP_SORT_AGGR 
        print *(path_sa_t*)$path
    end
    if $path->type == OP_DML_INSERT 
        print *(path_ins_t*)$path
    end
    if $path->type == OP_DML_DELETE
        print *(path_del_t*)$path
    end
    if $path->type == OP_DML_UPDATE
        print *(path_upd_t*)$path
    end
    if $path->type == OP_ISCAN_FULL 
        print *(path_ins_t*)$path
    end
    if $path->type == OP_ISCAN_RANGE
        print *(path_ins_t*)$path
    end
    if $path->type == OP_ISCAN_UNIQ
        print *(path_ins_t*)$path
    end
    if $path->type == OP_ISCAN_SKIP
        print *(path_ins_t*)$path
    end
    if $path->type == OP_ISCAN_FAST_FULL
        print *(path_ins_t*)$path
    end
    if $path->type == OP_CONNECTBY
        print *(path_cb_t*)$path
    end
    if $path->type == OP_CONNECTBY_STACK 
        print *(path_cb_t*)$path
    end
    if $path->type == OP_WINDOW
        print *(path_win_t*)$path
    end
    if $path->type == OP_BUFF
        print *(path_buff_t*)$path
    end
    if $path->type == OP_BUFF_TRANS
        print *(path_buff_t*)$path
    end
    if $path->type == OP_INLIST
        print *(path_inlist_t*)$path
    end
    if $path->type == OP_PE_MANAGER
        print *(path_pe_mgr_t*)$path
    end
    if $path->type == OP_PE_SEND_HASH
        print *(path_pe_send_t*)$path
    end
    if $path->type == OP_PE_SEND_RANGE
        print *(path_pe_send_t*)$path
    end
    if $path->type == OP_PE_SEND_BROAD
        print *(path_pe_send_t*)$path
    end
    if $path->type == OP_PE_SEND_RR
        print *(path_pe_send_t*)$path
    end
    if $path->type == OP_PE_SEND_QC_RAND
        print *(path_pe_send_t*)$path
    end
    if $path->type == OP_PE_SEND_QC_ORDER
        print *(path_pe_send_t*)$path
    end
    if $path->type == OP_PE_RECV
        print *(path_pe_recv_t*)$path
    end
    if $path->type == OP_PE_BLOCK_ITER
        print *(path_pe_block_iter_t*)$path
    end
end

define path
set $path = (path_t*)(&($arg0))
    path2 $path
end

define pchild
set $path = (path_t*)(&($arg0))
set $child = $path->l
    path2 $child
end

define pchild2
set $path = (path_t*)($arg0)
set $child = $path->l
    path2 $child
end

define expn
set $expn = (expn_t*)(&($arg0))
    if $expn->node_type == EXPN_OPERATOR
        print *(expn_op_t*)$expn
    end
    if $expn->node_type == EXPN_CONSTANT
        print *(expn_data_t*)$expn
    end
    if $expn->node_type == EXPN_COLUMN
        print *(expn_col_t*)$expn
    end
    if $expn->node_type == EXPN_SUBQRY_COLUMN
        print *(expn_subqry_col_t*)$expn
    end
    if $expn->node_type == EXPN_FUNCTION
        print *(expn_fn_t*)$expn
    end
    if $expn->node_type == EXPN_SUBQUERY
        print *(expn_subqry_t*)$expn
    end
    if $expn->node_type == EXPN_PARAMETER
        print *(expn_col_t*)$expn
    end
    if $expn->node_type == EXPN_TUPLE
        print *(expn_tuple_t*)$expn
    end
    if $expn->node_type == EXPN_UDF
        print *(expn_udf_t*)$expn
    end
    if $expn->node_type == EXPN_LP_COLUMN
        print *(expn_lp_column_t*)$expn
    end
    if $expn->node_type == EXPN_LP_SET_CLAUSE
        print *(expn_lp_set_clause_t*)$expn
    end
    if $expn->node_type == EXPN_LP_AN
        print *(expn_lp_an_t*)$expn
    end
    if $expn->node_type == EXPN_SEQ
        print *(expn_seq_t*)$expn
    end
    if $expn->node_type == EXPN_ROWNUM
        print *(expn_rownum_t*)$expn
    end
    if $expn->node_type == EXPN_LEVEL
        print *(expn_level_t*)$expn
    end
    if $expn->node_type == EXPN_CB_ISLEAF
        print *(expn_cb_isleaf_t*)$expn
    end
    if $expn->node_type == EXPN_CB_ISCYCLE
        print *(expn_cb_iscycle_t*)$expn
    end
    if $expn->node_type == EXPN_AGGR
        print *(expn_aggr_t*)$expn
    end
    if $expn->node_type == EXPN_SETCOL
        print *(expn_setcol_t*)$expn
    end
    if $expn->node_type == EXPN_AN
        print *(expn_an_t*)$expn
    end
    if $expn->node_type == EXPN_GRP_ID
        print *(expn_grp_id_t*)$expn
    end
    if $expn->node_type == EXPN_GRPING_ID
        print *(expn_grping_id_t*)$expn
    end
    if $expn->node_type == EXPN_PIVOT
        print *(expn_pivot_t*)$expn
    end
    if $expn->node_type == EXPN_DBLINK
        print *(expn_dblink_t*)$expn
    end
    if $expn->node_type == EXPN_ASSOC
        print *(expn_assoc_t*)$expn
    end
#if $expn->node_type == EXPN_OBJ_SETNULL
#       print *(expn_obj_setnull_t*)$expn
#   end
#    if $expn->node_type == EXPN_PARSE_OBJ_REF
#        print *(expn_parse_obj_ref_t*)$expn
#    end
#    if $expn->node_type == EXPN_DEREF
#        print *(expn_deref_t*)$expn
#    end
#    if $expn->node_type == EXPN_TREAT
#        print *(expn_treat_t*)$expn
#    end
#    if $expn->node_type == EXPN_IS_OF_TYPE
#        print *(expn_is_of_type_t*)$expn
#    end
#    if $expn->node_type == EXPN_NONE
#        print *(expn_t*)expn
#    end
#    if $expn->node_type == EXPN_MAX
#        print *(expn_t*)expn
#    end
end 


define expn2
set $expn = (expn_t *)$arg0
    if $expn->node_type == EXPN_OPERATOR
        print *(expn_op_t*)$expn
    end
    if $expn->node_type == EXPN_CONSTANT
        print *(expn_data_t*)$expn
    end
    if $expn->node_type == EXPN_COLUMN
        print *(expn_col_t*)$expn
    end
    if $expn->node_type == EXPN_SUBQRY_COLUMN
        print *(expn_subqry_col_t*)$expn
    end
    if $expn->node_type == EXPN_FUNCTION
        print *(expn_fn_t*)$expn
    end
    if $expn->node_type == EXPN_SUBQUERY
        print *(expn_subqry_t*)$expn
    end
    if $expn->node_type == EXPN_PARAMETER
        print *(expn_col_t*)$expn
    end
    if $expn->node_type == EXPN_TUPLE
        print *(expn_tuple_t*)$expn
    end
    if $expn->node_type == EXPN_UDF
        print *(expn_udf_t*)$expn
    end
    if $expn->node_type == EXPN_LP_COLUMN
        print *(expn_lp_column_t*)$expn
    end
    if $expn->node_type == EXPN_LP_SET_CLAUSE
        print *(expn_lp_set_clause_t*)$expn
    end
    if $expn->node_type == EXPN_LP_AN
        print *(expn_lp_an_t*)$expn
    end
    if $expn->node_type == EXPN_SEQ
        print *(expn_seq_t*)$expn
    end
    if $expn->node_type == EXPN_ROWNUM
        print *(expn_rownum_t*)$expn
    end
    if $expn->node_type == EXPN_LEVEL
        print *(expn_level_t*)$expn
    end
    if $expn->node_type == EXPN_CB_ISLEAF
        print *(expn_cb_isleaf_t*)$expn
    end
    if $expn->node_type == EXPN_CB_ISCYCLE
        print *(expn_cb_iscycle_t*)$expn
    end
    if $expn->node_type == EXPN_AGGR
        print *(expn_aggr_t*)$expn
    end
    if $expn->node_type == EXPN_SETCOL
        print *(expn_setcol_t*)$expn
    end
    if $expn->node_type == EXPN_AN
        print *(expn_an_t*)$expn
    end
    if $expn->node_type == EXPN_GRP_ID
        print *(expn_grp_id_t*)$expn
    end
    if $expn->node_type == EXPN_GRPING_ID
        print *(expn_grping_id_t*)$expn
    end
    if $expn->node_type == EXPN_PIVOT
        print *(expn_pivot_t*)$expn
    end
    if $expn->node_type == EXPN_DBLINK
        print *(expn_dblink_t*)$expn
    end
    if $expn->node_type == EXPN_ASSOC
        print *(expn_assoc_t*)$expn
    end
#if $expn->node_type == EXPN_OBJ_SETNULL
#       print *(expn_obj_setnull_t*)$expn
#   end
#    if $expn->node_type == EXPN_PARSE_OBJ_REF
#        print *(expn_parse_obj_ref_t*)$expn
#    end
#    if $expn->node_type == EXPN_DEREF
#        print *(expn_deref_t*)$expn
#    end
#    if $expn->node_type == EXPN_TREAT
#        print *(expn_treat_t*)$expn
#    end
#    if $expn->node_type == EXPN_IS_OF_TYPE
#        print *(expn_is_of_type_t*)$expn
#    end
#    if $expn->node_type == EXPN_NONE
#        print *(expn_t*)expn
#    end
#    if $expn->node_type == EXPN_MAX
#        print *(expn_t*)expn
#    end
end 

define pexp                                                                               
    print sess_info_ [tb_get_tid()].opt_ctx->exps[$arg0]
end

define poctx                                                                              
    print sess_info_ [tb_get_tid()].opt_ctx
end

define poctx2                                                                                     
    print sess_info_ [tb_get_sessid()].opt_ctx                                              
end

define psess
    print sess_info_[tb_get_sessid()]
end

define ps
    print ($arg0)._M_ptr
end

define bo
    b optimizer::OptimizerCm::Optimize()
end

define test
    run --gtest_filter=SqlCommonTestFixture.Temp_Test
end

handle SIG34 nostop
handle SIG34 noprint
handle SIGUSR2 nostop
handle SIGUSR2 noprint
#                                                                                         
# @brief    Listc print                                                                   
# ex) (gdb) print_listc listc type                                                        
#                                                                                         
#                                                                                         
# @param    $arg0 list                                                                    
# @param    $arg1 type                                                                    
#                                                                                         
define plc                                                                                
    set $_pos=(listc_t *)($arg0)->link.next
    while ($_pos != ($arg0))
        print $_pos->data
        print *($arg1 *)$_pos->data
        set $_pos = (listc_t *)$_pos->link.next
    end
end

define plc_num
    set $_pos=(listc_t *)($arg0)->link.next
    while ($_pos != ($arg0))
        print $_pos->data
        print (int)($_pos->data)
        set $_pos = (listc_t *)$_pos->link.next
    end
end

#                                                                                         
# @brief    link_t�� �̿��� link������ print���ִ� �Լ�                                   
# ex) (gdb) print_link_nth list type_t member_name 3                                      
#                                                                                         
#                                                                                         
# @param    $arg0 list                                                                    
# @param    $arg1 type                                                                    
# @param    $arg2 link member �̸�                                                         
# @param    $arg3 n-th element (1,2,3....)                                                
#                                                                                         
define plnth                                                                              
    set $_cnt_=0                                                                          
    set $_ptr_=$arg0                                                                      
    set $_offset_=(unsigned int)(&((($arg1 *)0)->$arg2))                                  
                                                                                          
    while ($_cnt_ < $arg3)                                                                
        set $_ptr_=$_ptr_->next                                                           
        set $_cnt_++                                                                      
    end                                                                                   
                                                                                          
    if ($arg3 > 0)                                                                        
        set $_ptr_=($arg1 *)((char *)$_ptr_ - $_offset_)                                  
    end                                                                                   

    print $_ptr_
    print *$_ptr_
end

# @brief    link_t�� �̿��� link������ print���ִ� �Լ�                                   
# ex) (gdb) print_link list type_t member_name 3                                          
#                                                                                         
#                                                                                         
# @param    $arg0 list                                                                    
# @param    $arg1 type                                                                    
# @param    $arg2 link member �̸�                                                        
#                                                                                         
define pl                                                                                 
    set $_pos=($arg0)->next

    while ($_pos != ($arg0))
        set $_ptr_=($arg1 *)((char *)$_pos - (char *)&((($arg1 *)0)->$arg2))
        print $_ptr_
        print *$_ptr_
        set $_pos = $_pos->next
    end
end

#                                                                                         
# @brief    ���� Link print                                                               
# ex) (gdb) print_link list type_t member_name                                            
#                                                                                         
#                                                                                         
# @param    $arg0 list                                                                    
# @param    $arg1 type                                                                    
# @param    $arg2 link member �̸�                                                        
#                                                                                         
define pln                                                                                
    print ($arg1 *)((char *)(($arg1 *)($arg0))->$arg2.next - \
                   (unsigned int)(&((($arg1 *)0)->$arg2)))
end

#
#   The new GDB commands:                                                         
#       are entirely non instrumental                                             
#       do not depend on any "inline"(s) - e.g. size(), [], etc
#       are extremely tolerant to debugger settings
#                                                                                 
#   This file should be "included" in .gdbinit as following:
#   source stl-views.gdb or just paste it into your .gdbinit file
#
#   The following STL containers are currently supported:
#
#       std::vector<T> -- via pvector command
#       std::list<T> -- via plist or plist_member command
#       std::map<T,T> -- via pmap or pmap_member command
#       std::multimap<T,T> -- via pmap or pmap_member command
#       std::set<T> -- via pset command
#       std::multiset<T> -- via pset command
#       std::deque<T> -- via pdequeue command
#       std::stack<T> -- via pstack command
#       std::queue<T> -- via pqueue command
#       std::priority_queue<T> -- via ppqueue command
#       std::bitset<n> -- via pbitset command
#       std::string -- via pstring command
#       std::widestring -- via pwstring command
#
#   The end of this file contains (optional) C++ beautifiers
#   Make sure your debugger supports $argc
#
#   Simple GDB Macros writen by Dan Marinescu (H-PhD) - License GPL
#   Inspired by intial work of Tom Malnar, 
#     Tony Novac (PhD) / Cornell / Stanford,
#     Gilad Mishne (PhD) and Many Many Others.
#   Contact: dan_c_marinescu@yahoo.com (Subject: STL)
#
#   Modified to work with g++ 4.3 by Anders Elton
#   Also added _member functions, that instead of printing the entire class in map, prints a member.



#
# std::vector<>
#

define pvector
    if $argc == 0
        help pvector
    else
        set $size = $arg0._M_impl._M_finish - $arg0._M_impl._M_start
        set $capacity = $arg0._M_impl._M_end_of_storage - $arg0._M_impl._M_start
        set $size_max = $size - 1
    end
    if $argc == 1
        set $i = 0
        while $i < $size
            printf "elem[%u]: ", $i
            p *($arg0._M_impl._M_start + $i)
            set $i++
        end
    end
    if $argc == 2
        set $idx = $arg1
        if $idx < 0 || $idx > $size_max
            printf "idx1, idx2 are not in acceptable range: [0..%u].\n", $size_max
        else
            printf "elem[%u]: ", $idx
            p *($arg0._M_impl._M_start + $idx)
        end
    end
    if $argc == 3
      set $start_idx = $arg1
      set $stop_idx = $arg2
      if $start_idx > $stop_idx
        set $tmp_idx = $start_idx
        set $start_idx = $stop_idx
        set $stop_idx = $tmp_idx
      end
      if $start_idx < 0 || $stop_idx < 0 || $start_idx > $size_max || $stop_idx > $size_max
        printf "idx1, idx2 are not in acceptable range: [0..%u].\n", $size_max
      else
        set $i = $start_idx
        while $i <= $stop_idx
            printf "elem[%u]: ", $i
            p *($arg0._M_impl._M_start + $i)
            set $i++
        end
      end
    end
    if $argc > 0
        printf "Vector size = %u\n", $size
        printf "Vector capacity = %u\n", $capacity
        printf "Element "
        whatis $arg0._M_impl._M_start
    end
end

document pvector
    Prints std::vector<T> information.
    Syntax: pvector <vector> <idx1> <idx2>
    Note: idx, idx1 and idx2 must be in acceptable range [0..<vector>.size()-1].
    Examples:
    pvector v - Prints vector content, size, capacity and T typedef
    pvector v 0 - Prints element[idx] from vector
    pvector v 1 2 - Prints elements in range [idx1..idx2] from vector
end 

#
# std::list<>
#

define plist
    if $argc == 0
        help plist
    else
        set $head = &$arg0._M_impl._M_node
        set $current = $arg0._M_impl._M_node._M_next
        set $size = 0
        while $current != $head
            if $argc == 2
                printf "elem[%u]: ", $size
                p *($arg1*)($current + 1)
            end
            if $argc == 3
                if $size == $arg2
                    printf "elem[%u]: ", $size
                    p *($arg1*)($current + 1)
                end
            end
            set $current = $current._M_next
            set $size++
        end
        printf "List size = %u \n", $size
        if $argc == 1
            printf "List "
            whatis $arg0
            printf "Use plist <variable_name> <element_type> to see the elements in the list.\n"
        end
    end
end

document plist
    Prints std::list<T> information.
    Syntax: plist <list> <T> <idx>: Prints list size, if T defined all elements or just element at idx
    Examples:
    plist l - prints list size and definition
    plist l int - prints all elements and list size
    plist l int 2 - prints the third element in the list (if exists) and list size
end

define plist_member
    if $argc == 0
        help plist_member
    else
        set $head = &$arg0._M_impl._M_node
        set $current = $arg0._M_impl._M_node._M_next
        set $size = 0
        while $current != $head
            if $argc == 3
                printf "elem[%u]: ", $size
                p (*($arg1*)($current + 1)).$arg2
            end
            if $argc == 4
                if $size == $arg3
                    printf "elem[%u]: ", $size
                    p (*($arg1*)($current + 1)).$arg2
                end
            end
            set $current = $current._M_next
            set $size++
        end
        printf "List size = %u \n", $size
        if $argc == 1
            printf "List "
            whatis $arg0
            printf "Use plist_member <variable_name> <element_type> <member> to see the elements in the list.\n"
        end
    end
end

document plist_member
    Prints std::list<T> information.
    Syntax: plist <list> <T> <idx>: Prints list size, if T defined all elements or just element at idx
    Examples:
    plist_member l int member - prints all elements and list size
    plist_member l int member 2 - prints the third element in the list (if exists) and list size
end


#
# std::map and std::multimap
#

define pmap
    if $argc == 0
        help pmap
    else
        set $tree = $arg0
        set $i = 0
        set $node = $tree._M_t._M_impl._M_header._M_left
        set $end = $tree._M_t._M_impl._M_header
        set $tree_size = $tree._M_t._M_impl._M_node_count
        if $argc == 1
            printf "Map "
            whatis $tree
            printf "Use pmap <variable_name> <left_element_type> <right_element_type> to see the elements in the map.\n"
        end
        if $argc == 3
            while $i < $tree_size
                set $value = (void *)($node + 1)
                printf "elem[%u].left: ", $i
                p *($arg1*)$value
                set $value = $value + sizeof($arg1)
                printf "elem[%u].right: ", $i
                p *($arg2*)$value
                if $node._M_right != 0
                    set $node = $node._M_right
                    while $node._M_left != 0
                        set $node = $node._M_left
                    end
                else
                    set $tmp_node = $node._M_parent
                    while $node == $tmp_node._M_right
                        set $node = $tmp_node
                        set $tmp_node = $tmp_node._M_parent
                    end
                    if $node._M_right != $tmp_node
                        set $node = $tmp_node
                    end
                end
                set $i++
            end
        end
        if $argc == 4
            set $idx = $arg3
            set $ElementsFound = 0
            while $i < $tree_size
                set $value = (void *)($node + 1)
                if *($arg1*)$value == $idx
                    printf "elem[%u].left: ", $i
                    p *($arg1*)$value
                    set $value = $value + sizeof($arg1)
                    printf "elem[%u].right: ", $i
                    p *($arg2*)$value
                    set $ElementsFound++
                end
                if $node._M_right != 0
                    set $node = $node._M_right
                    while $node._M_left != 0
                        set $node = $node._M_left
                    end
                else
                    set $tmp_node = $node._M_parent
                    while $node == $tmp_node._M_right
                        set $node = $tmp_node
                        set $tmp_node = $tmp_node._M_parent
                    end
                    if $node._M_right != $tmp_node
                        set $node = $tmp_node
                    end
                end
                set $i++
            end
            printf "Number of elements found = %u\n", $ElementsFound
        end
        if $argc == 5
            set $idx1 = $arg3
            set $idx2 = $arg4
            set $ElementsFound = 0
            while $i < $tree_size
                set $value = (void *)($node + 1)
                set $valueLeft = *($arg1*)$value
                set $valueRight = *($arg2*)($value + sizeof($arg1))
                if $valueLeft == $idx1 && $valueRight == $idx2
                    printf "elem[%u].left: ", $i
                    p $valueLeft
                    printf "elem[%u].right: ", $i
                    p $valueRight
                    set $ElementsFound++
                end
                if $node._M_right != 0
                    set $node = $node._M_right
                    while $node._M_left != 0
                        set $node = $node._M_left
                    end
                else
                    set $tmp_node = $node._M_parent
                    while $node == $tmp_node._M_right
                        set $node = $tmp_node
                        set $tmp_node = $tmp_node._M_parent
                    end
                    if $node._M_right != $tmp_node
                        set $node = $tmp_node
                    end
                end
                set $i++
            end
            printf "Number of elements found = %u\n", $ElementsFound
        end
        printf "Map size = %u\n", $tree_size
    end
end

document pmap
    Prints std::map<TLeft and TRight> or std::multimap<TLeft and TRight> information. Works for std::multimap as well.
    Syntax: pmap <map> <TtypeLeft> <TypeRight> <valLeft> <valRight>: Prints map size, if T defined all elements or just element(s) with val(s)
    Examples:
    pmap m - prints map size and definition
    pmap m int int - prints all elements and map size
    pmap m int int 20 - prints the element(s) with left-value = 20 (if any) and map size
    pmap m int int 20 200 - prints the element(s) with left-value = 20 and right-value = 200 (if any) and map size
end


define pmap_member
    if $argc == 0
        help pmap_member
    else
        set $tree = $arg0
        set $i = 0
        set $node = $tree._M_t._M_impl._M_header._M_left
        set $end = $tree._M_t._M_impl._M_header
        set $tree_size = $tree._M_t._M_impl._M_node_count
        if $argc == 1
            printf "Map "
            whatis $tree
            printf "Use pmap <variable_name> <left_element_type> <right_element_type> to see the elements in the map.\n"
        end
        if $argc == 5
            while $i < $tree_size
                set $value = (void *)($node + 1)
                printf "elem[%u].left: ", $i
                p (*($arg1*)$value).$arg2
                set $value = $value + sizeof($arg1)
                printf "elem[%u].right: ", $i
                p (*($arg3*)$value).$arg4
                if $node._M_right != 0
                    set $node = $node._M_right
                    while $node._M_left != 0
                        set $node = $node._M_left
                    end
                else
                    set $tmp_node = $node._M_parent
                    while $node == $tmp_node._M_right
                        set $node = $tmp_node
                        set $tmp_node = $tmp_node._M_parent
                    end
                    if $node._M_right != $tmp_node
                        set $node = $tmp_node
                    end
                end
                set $i++
            end
        end
        if $argc == 6
            set $idx = $arg5
            set $ElementsFound = 0
            while $i < $tree_size
                set $value = (void *)($node + 1)
                if *($arg1*)$value == $idx
                    printf "elem[%u].left: ", $i
                    p (*($arg1*)$value).$arg2
                    set $value = $value + sizeof($arg1)
                    printf "elem[%u].right: ", $i
                    p (*($arg3*)$value).$arg4
                    set $ElementsFound++
                end
                if $node._M_right != 0
                    set $node = $node._M_right
                    while $node._M_left != 0
                        set $node = $node._M_left
                    end
                else
                    set $tmp_node = $node._M_parent
                    while $node == $tmp_node._M_right
                        set $node = $tmp_node
                        set $tmp_node = $tmp_node._M_parent
                    end
                    if $node._M_right != $tmp_node
                        set $node = $tmp_node
                    end
                end
                set $i++
            end
            printf "Number of elements found = %u\n", $ElementsFound
        end
        printf "Map size = %u\n", $tree_size
    end
end

document pmap_member
    Prints std::map<TLeft and TRight> or std::multimap<TLeft and TRight> information. Works for std::multimap as well.
    Syntax: pmap <map> <TtypeLeft> <TypeRight> <valLeft> <valRight>: Prints map size, if T defined all elements or just element(s) with val(s)
    Examples:
    pmap_member m class1 member1 class2 member2 - prints class1.member1 : class2.member2
    pmap_member m class1 member1 class2 member2 lvalue - prints class1.member1 : class2.member2 where class1 == lvalue
end


#
# std::set and std::multiset
#

define pset
    if $argc == 0
        help pset
    else
        set $tree = $arg0
        set $i = 0
        set $node = $tree._M_t._M_impl._M_header._M_left
        set $end = $tree._M_t._M_impl._M_header
        set $tree_size = $tree._M_t._M_impl._M_node_count
        if $argc == 1
            printf "Set "
            whatis $tree
            printf "Use pset <variable_name> <element_type> to see the elements in the set.\n"
        end
        if $argc == 2
            while $i < $tree_size
                set $value = (void *)($node + 1)
                printf "elem[%u]: ", $i
                p *($arg1*)$value
                if $node._M_right != 0
                    set $node = $node._M_right
                    while $node._M_left != 0
                        set $node = $node._M_left
                    end
                else
                    set $tmp_node = $node._M_parent
                    while $node == $tmp_node._M_right
                        set $node = $tmp_node
                        set $tmp_node = $tmp_node._M_parent
                    end
                    if $node._M_right != $tmp_node
                        set $node = $tmp_node
                    end
                end
                set $i++
            end
        end
        if $argc == 3
            set $idx = $arg2
            set $ElementsFound = 0
            while $i < $tree_size
                set $value = (void *)($node + 1)
                if *($arg1*)$value == $idx
                    printf "elem[%u]: ", $i
                    p *($arg1*)$value
                    set $ElementsFound++
                end
                if $node._M_right != 0
                    set $node = $node._M_right
                    while $node._M_left != 0
                        set $node = $node._M_left
                    end
                else
                    set $tmp_node = $node._M_parent
                    while $node == $tmp_node._M_right
                        set $node = $tmp_node
                        set $tmp_node = $tmp_node._M_parent
                    end
                    if $node._M_right != $tmp_node
                        set $node = $tmp_node
                    end
                end
                set $i++
            end
            printf "Number of elements found = %u\n", $ElementsFound
        end
        printf "Set size = %u\n", $tree_size
    end
end

document pset
    Prints std::set<T> or std::multiset<T> information. Works for std::multiset as well.
    Syntax: pset <set> <T> <val>: Prints set size, if T defined all elements or just element(s) having val
    Examples:
    pset s - prints set size and definition
    pset s int - prints all elements and the size of s
    pset s int 20 - prints the element(s) with value = 20 (if any) and the size of s
end



#
# std::dequeue
#

define pdequeue
    if $argc == 0
        help pdequeue
    else
        set $size = 0
        set $start_cur = $arg0._M_impl._M_start._M_cur
        set $start_last = $arg0._M_impl._M_start._M_last
        set $start_stop = $start_last
        while $start_cur != $start_stop
            p *$start_cur
            set $start_cur++
            set $size++
        end
        set $finish_first = $arg0._M_impl._M_finish._M_first
        set $finish_cur = $arg0._M_impl._M_finish._M_cur
        set $finish_last = $arg0._M_impl._M_finish._M_last
        if $finish_cur < $finish_last
            set $finish_stop = $finish_cur
        else
            set $finish_stop = $finish_last
        end
        while $finish_first != $finish_stop
            p *$finish_first
            set $finish_first++
            set $size++
        end
        printf "Dequeue size = %u\n", $size
    end
end

document pdequeue
    Prints std::dequeue<T> information.
    Syntax: pdequeue <dequeue>: Prints dequeue size, if T defined all elements
    Deque elements are listed "left to right" (left-most stands for front and right-most stands for back)
    Example:
    pdequeue d - prints all elements and size of d
end



#
# std::stack
#

define pstack
    if $argc == 0
        help pstack
    else
        set $start_cur = $arg0.c._M_impl._M_start._M_cur
        set $finish_cur = $arg0.c._M_impl._M_finish._M_cur
        set $size = $finish_cur - $start_cur
        set $i = $size - 1
        while $i >= 0
            p *($start_cur + $i)
            set $i--
        end
        printf "Stack size = %u\n", $size
    end
end

document pstack
    Prints std::stack<T> information.
    Syntax: pstack <stack>: Prints all elements and size of the stack
    Stack elements are listed "top to buttom" (top-most element is the first to come on pop)
    Example:
    pstack s - prints all elements and the size of s
end



#
# std::queue
#

define pqueue
    if $argc == 0
        help pqueue
    else
        set $start_cur = $arg0.c._M_impl._M_start._M_cur
        set $finish_cur = $arg0.c._M_impl._M_finish._M_cur
        set $size = $finish_cur - $start_cur
        set $i = 0
        while $i < $size
            p *($start_cur + $i)
            set $i++
        end
        printf "Queue size = %u\n", $size
    end
end

document pqueue
    Prints std::queue<T> information.
    Syntax: pqueue <queue>: Prints all elements and the size of the queue
    Queue elements are listed "top to bottom" (top-most element is the first to come on pop)
    Example:
    pqueue q - prints all elements and the size of q
end



#
# std::priority_queue
#

define ppqueue
    if $argc == 0
        help ppqueue
    else
        set $size = $arg0.c._M_impl._M_finish - $arg0.c._M_impl._M_start
        set $capacity = $arg0.c._M_impl._M_end_of_storage - $arg0.c._M_impl._M_start
        set $i = $size - 1
        while $i >= 0
            p *($arg0.c._M_impl._M_start + $i)
            set $i--
        end
        printf "Priority queue size = %u\n", $size
        printf "Priority queue capacity = %u\n", $capacity
    end
end

document ppqueue
    Prints std::priority_queue<T> information.
    Syntax: ppqueue <priority_queue>: Prints all elements, size and capacity of the priority_queue
    Priority_queue elements are listed "top to buttom" (top-most element is the first to come on pop)
    Example:
    ppqueue pq - prints all elements, size and capacity of pq
end



#
# std::bitset
#

define pbitset
    if $argc == 0
        help pbitset
    else
        p /t $arg0._M_w
    end
end

document pbitset
    Prints std::bitset<n> information.
    Syntax: pbitset <bitset>: Prints all bits in bitset
    Example:
    pbitset b - prints all bits in b
end



#
# std::string
#

define pstring
    if $argc == 0
        help pstring
    else
        printf "String \t\t\t= \"%s\"\n", $arg0._M_data()
        printf "String size/length \t= %u\n", $arg0._M_rep()._M_length
        printf "String capacity \t= %u\n", $arg0._M_rep()._M_capacity
        printf "String ref-count \t= %d\n", $arg0._M_rep()._M_refcount
    end
end

document pstring
    Prints std::string information.
    Syntax: pstring <string>
    Example:
    pstring s - Prints content, size/length, capacity and ref-count of string s
end 

#
# std::wstring
#

define pwstring
    if $argc == 0
        help pwstring
    else
        call printf("WString \t\t= \"%ls\"\n", $arg0._M_data())
        printf "WString size/length \t= %u\n", $arg0._M_rep()._M_length
        printf "WString capacity \t= %u\n", $arg0._M_rep()._M_capacity
        printf "WString ref-count \t= %d\n", $arg0._M_rep()._M_refcount
    end
end

document pwstring
    Prints std::wstring information.
    Syntax: pwstring <wstring>
    Example:
    pwstring s - Prints content, size/length, capacity and ref-count of wstring s
end 

#
# C++ related beautifiers (optional)
#

set print object on
set print static-members on
set print vtbl on
set print demangle on
set demangle-style gnu-v3
set print sevenbit-strings off

