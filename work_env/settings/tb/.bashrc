# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
#if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
#    . /etc/bash_completion
#fi

export LC_ALL=ko_KR.utf-8
alias bashrc='vim ~/.bashrc'
alias bs='source ~/.bashrc'
export TB_BASE='/Tibero'
export TB_HOME="/Tibero/tibero7"
export TB_SID="tibero"
export use_cdpath=true

export JAVA_HOME=/usr/local/java/jdk1.7.0_80
export JRE_HOME=/usr/local/java/jdk1.7.0_80
export PATH_INIT="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$JRE_HOME/bin:$JAVA_HOME/bin"
export PATH=$PATH_INIT
export PATH_ORIG=$PATH
export LD_LIBRARY_PATH_ORIG=$LD_LIBRARY_PATH


# 특정 버전의 git repository로 이동
# (함수 내 디렉토리들은 자신의 환경에 맞게 수정할 것)
# usage : tb <이동하고자 하는 git  repo의 디렉토리명>
# 예 : tb tibero7
function tb()
{
if [ -f $TB_BASE/$1/tbenv ]; then
    tbroot=$TB_BASE'/'$1;
elif [ -f $1/tbenv ]; then
    tbroot=$1;
else
    echo Error: Invalid Directory
    return
fi
echo "tbroot Path: $tbroot"

[ -f $tbroot/dev-util/bashrc.db1team ] &&  . $tbroot/dev-util/bashrc.db1team
#set_tb_home
#set_tb_sid
#set_tb_path
exec_tbenv
#export TB_HOME=`pwd`
#export CM_HOME=$TB_HOME
#export TBGW_HOME=$TB_HOME/client/gateway
#export TB_LNS_LANG=UTF8
}
function exec_tbenv()
{
  echo "Execute tbenv"
  cd $tbroot
  . tbenv $tbroot
}

# SET함수들 사용 중단.
# set_tb_path / set_tb_home / set_tb_sid
# 그냥 각 버전에 맞는 tbenv를 실행하도록 변경
# 값이 제대로 업데이트가 안되서 그런지 얘네로 Set하면 jam autoconf 할 때 에러남
function set_tb_path()
{
  export LD_LIBRARY_PATH="$TB_HOME/lib:$TB_HOME/client/lib"
  PATH="$PATH_INIT:$TB_HOME/scripts:$TB_HOME/src/scripts:$TB_HOME/bin:$TB_HOME/client/bin:$TB_HOME/tools/bin/"
  PATH_ORIG=$PATH
  echo "Update tb lib path"
}


function set_tb_home()
{
  export TB_HOME=$tbroot
  echo "Set TB_HOME: $TB_HOME"
}
function set_tb_sid()
{
  if [ x"$TB_SID" != "x" ];then
    export TB_SID=$TB_SID
  elif [ x"$tb_sid" != "x" ];then
    export TB_SID=$tb_sid
  fi
  echo "Set TB_SID: $TB_SID"
}
function set_tb()
{
#  if [[ -z $tbroot ]]; then
    echo "tbroot is Empty"
    if [[ `pwd` =~ .*tibero6.* ]]; then
      echo "tbroot set to tibero6."
      tb "tibero6"
    elif [[ `pwd` =~ .*tibero7.* ]]; then
      echo "tbroot set to tibero7."
      tb "tibero7"
    else
      echo "tbroot set failed."
      echo "Set tbroot path using 'cd6' or 'cd7'"
      echo "or using 'set_tb' in a Tibero Dir[tibero6/tibero7]."
    fi
#  elif [[ -n $tbroot ]]; then
#    echo "tbroot is not Empty"
#  fi

## expr을 이용한 String 비교. 아래에서 word가 Char단위로 비교해서 못씀...
#  str=$tbroot
#  word="tibero6"
#  expr index "$str" "$word"
}
# ims 이슈 관련 자료(재현 시나리오 등)를 모아 두는 디렉토리로 이동
# 이슈 번호까지 입력하면 해당 이슈에 대한 디렉토리로 이동하며,
# 디렉토리가 없는 경우 생성
# 예) ims        : ims 디렉토리로 이동
#     ims 123456 : ims/12/123456 디렉토리로 이동
function ims()
{
    dst=$1;

    cd $1/ims

    if [ -z $dst ] ; then
        return;
    fi

    # dst가 이슈 번호인 경우, 뒤 4자리를 자른 것이 idx가 된다.
    # 예) 123456 -> 12
    re='^[0-9]+$';
    if [[ $dst =~ $re ]] && [ ${#dst} -ge 5 ] ; then
        idx=`echo "${dst:0:-4}"`;
    else
        idx=$dst;
    fi

    if [ ! -d $idx ]; then
        mkdir $idx
    fi
    cd $idx

    if [ $dst = $idx ] ; then
        return;
    elif [ ! -d $dst ]; then
        mkdir $dst
    fi
    cd $dst
}

alias vol='cd $TB_BASE'
alias cd6='tb "tibero6" && cd $tbroot'
alias cd7='tb "tibero7" && cd $tbroot'
alias cdt7='tb "testtibero7" && cd $tbroot'
alias tb6='cd6';
alias tb7='cd7';
alias tbsql='rlwrap tbsql'
#alias xgrep='find . -type f | xargs grep -I --color=always'
alias gdbinit='cd /etc/gdb/gdbinit'
alias re='tbdown abnormal && tbboot'
alias jb='cd $TB_HOME/src && jam -j12 && cd $OLDPWD'
alias patchall='$TB_HOME/dev-util/patch-apply.sh'
alias repatchall='cd $TB_HOME; git reset --hard HEAD; git clean -fd *; $TB_HOME/dev-util/patch-apply.sh'
alias logtree='git log --branches --decorate --graph --oneline'
