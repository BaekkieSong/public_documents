conn / as sysdba
exec dbms_redact.drop_policy(object_schema=>'TIBERO',object_name=>'TB_REDACT_TEST',policy_name=>'redact_policy_tibero');
exec dbms_redact.drop_policy_expression(policy_expression_name=>'redact_expr_policy_tibero');

conn system/oracle
drop user tibero cascade;
drop user mask_user cascade;
create user tibero identified by tmax;
grant dba to tibero;

create table tibero.tb_redact_test (c1 number, c2 varchar(13), c3 date);
insert into tibero.tb_redact_test values (12345, '1111112222222', sysdate);

-- For Tibero
create index tibero.fbi_tb_redact_test on tibero.tb_redact_test(c1+c2);

create user mask_user identified by tibero;
grant select on tibero.tb_redact_test to mask_user;
grant create table, resource to mask_user;
grant connect to mask_user;
alter user mask_user default tablespace users quota unlimited on users;

conn / as sysdba
exec dbms_redact.add_policy(object_schema=>'TIBERO',object_name=>'TB_REDACT_TEST',column_name=>'C2',policy_name=>'redact_policy_tibero',function_type=>DBMS_REDACT.PARTIAL, function_parameters=>'VVVVVVVVVVVVV,VVVVVVVVVVVVV,*,8,13', expression=>'0=1');
select * from (select substr(c2,7) SUB_TIBERO from tibero.tb_redact_test);
conn mask_user/tibero
select * from (select substr(c2,7) SUB_MASK_USER from tibero.tb_redact_test);

conn / as sysdba
exec dbms_redact.create_policy_expression('redact_expr_policy_tibero', 'SYS_CONTEXT(''USERENV'',''SESSION_USER'') not in (''TIBERO'')');
--exec dbms_redact.update_policy_expression('redact_expr_policy_tibero', 'SYS_CONTEXT(''USERENV'',''SESSION_USER'') not in (''TIBERO'', ''MASK_USER'')');
exec dbms_redact.apply_policy_expr_to_col(object_schema=>'TIBERO',object_name=>'TB_REDACT_TEST',column_name=>'C2',policy_expression_name=>'redact_expr_policy_tibero');

conn mask_user/tibero
select substr(c2,7) SUB_MASK_USER from tibero.tb_redact_test;
select * from (select substr(c2,7) SUB_MASK_USER from tibero.tb_redact_test);
