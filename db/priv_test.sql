conn tibero/tmax
spool result.txt
set echo on

-- USAGE1. TOP SELECT
conn mask_user/tibero
select substr(c2,7) from tibero.tb_redact_test; 
-- 2******
-- has_priv : 2222222

-- USAGE2. INLINE VIEW
conn mask_user/tibero
select * from (select substr(c2,1,6) || '-' || substr(c2,7) from tibero.tb_redact_test);
-- ORA-28094 : SQL construct not supported by data redaction
-- has_priv : 111111-2222222
select * from (select a from (select substr(c2,7) a from tibero.tb_redact_test));
-- ORA-28094 : SQL construct not supported by data redaction
-- has_priv : 2222222

-- USAGE2-2. INLINE VIEW (Not used in proj)
select 1 from (select * from (select substr(c2,7) from tibero.tb_redact_test));
-- 1
-- has_priv : 1

-- USAGE3. WITH
conn mask_user/tibero
with w as (select c2 from tibero.tb_redact_test) select * from w;
-- 1111112******
-- has_priv : 1111112222222
with w as (select substr(c2,7) from tibero.tb_redact_test) select * from w;
-- ORA-28094 : SQL construct not supported by data redaction
-- has_priv : 2222222

-- USAGE4. SCALAR SUBQUERY
conn mask_user/tibero
select (select substr(c2,1,6) || '-' || substr(c2,7) from tibero.tb_redact_test) from dual;
-- 111111-2******
select (select * from (select substr(c2,7) from tibero.tb_redact_test)) from dual;
-- ORA-28094: SQL construct not supported by data redaction

-- USAGE5. MULTI_SELECT
conn mask_user/tibero
drop table multi_tbl;
create table multi_tbl as select level c1 from dual connect by level <= 5;
select (select substr(c2,7) from tibero.tb_redact_test) from multi_tbl;
-- 2****** (5 rows)

-- USAGE6. UDF
conn tibero/tmax
create or replace function my_func return varchar2 as x varchar(13);
begin select c2 into x from tibero.tb_redact_test; return x; end;
/
grant execute on tibero.my_func to mask_user;
conn mask_user/tibero
select * from (select tibero.my_func() UDF1 from dual);
-- 1111112******
-- has_priv : 1111112222222

-- USAGE6-2. UDF(+INLINE VIEW)
conn tibero/tmax
create or replace function my_func return varchar2 as x varchar(13);
begin select * into x from (select substr(c2,7) from tibero.tb_redact_test); return x; end;
/
-- Function created.
grant execute on tibero.my_func to mask_user;
conn mask_user/tibero
select * from (select tibero.my_func() UDF2 from dual);
-- ORA-28094: SQL construct not supported by data redaction 

-- USAGE6-3. INSERT + UDF
conn tibero/tmax
create or replace function my_func return varchar2 as x varchar(13);
begin select c2 into x from tibero.tb_redact_test; return x; end;
/
grant execute on tibero.my_func to mask_user;
conn mask_user/tibero
drop table insert_tbl;
create table insert_tbl (c2 varchar2(13));
insert into insert_tbl select tibero.my_func() UDF3 from dual;
select c2 UDF3_MASK from mask_user.insert_tbl;
-- 1111112******
conn tibero/tmax
select c2 UDF3_PRIV from mask_user.insert_tbl;
-- 1111112******

-- USAGE6-4. UDF(INPUT redactcol)
conn tibero/tmax
create or replace function my_func(input varchar2) return varchar2 
as x varchar2(13);
begin
select substr(input,7) into x from dual;
return x;
end;
/
grant execute on tibero.my_func to mask_user;
conn mask_user/tibero
select * from (select tibero.my_func(c2) UDF4 from tibero.tb_redact_test);
-- 2******


-- USAGE7. GROUP BY
conn mask_user/tibero
select substr(c2,7) from tibero.tb_redact_test group by substr(c2,7);
-- ORA-00979: not a GROUP BY expression
-- has_priv : 2222222
select substr(c2,7) from tibero.tb_redact_test group by c2;
-- 2******

-- USAGE8. ORDER BY
conn mask_user/tibero
select substr(c2,7) from tibero.tb_redact_test order by substr(c2,7);
-- 2******
select substr(c2,7) from tibero.tb_redact_test order by substr(c2,7) asc;
-- 2******
select substr(c2,7) from tibero.tb_redact_test order by c2;
-- 2******

-- USAGE9. CTAS
conn mask_user/tibero
create table ctas_tbl as select c2 from tibero.tb_redact_test;
-- ORA-28081: Insufficient privileges - the command references a redacted object.

-- USAGE10. DML
conn tibero/tmax
grant insert on tibero.tb_redact_test to mask_user;
grant update on tibero.tb_redact_test to mask_user;
grant delete on tibero.tb_redact_test to mask_user;
conn mask_user/tibero
insert into tibero.tb_redact_test values (2222222222, '3333334444444', sysdate);
-- 1 row created.
delete from tibero.tb_redact_test where c2='3333334444444';
-- 1 row deleted.
update tibero.tb_redact_test set c1=1111111111 where c2 = '1111112222222';
-- 1 row updated.
conn tibero/tmax
revoke insert on tibero.tb_redact_test from mask_user;
revoke update on tibero.tb_redact_test from mask_user;
revoke delete on tibero.tb_redact_test from mask_user;

-- USAGE11. DML + SELECT
conn mask_user/tibero
drop table insert_tbl;
create table insert_tbl (insert_col varchar2(13));
insert into insert_tbl select c2 from tibero.tb_redact_test;
-- ORA-28081: Insufficient privileges - the command references a redacted object.
insert into insert_tbl select substr(c2,7) from tibero.tb_redact_test;
-- ORA-28081: Insufficient privileges - the command references a redacted object.
update insert_tbl set insert_col='3333334444444' where insert_col = (select c2 from tibero.tb_redact_test where c2= '1111112222222');
-- ORA-28081: Insufficient privileges - the command references a redacted object.
delete from insert_tbl where insert_col = (select c2 from tibero.tb_redact_test where c2= '1111112222222');
-- ORA-28081: Insufficient privileges - the command references a redacted object.
select * from insert_tbl;
-- no rows selected
merge into insert_tbl a using tibero.tb_redact_test b on ('1111112222222' = (select max(c2) from tibero.tb_redact_test)) when not matched then insert (insert_col) values (c2);
-- ORA-28081: Insufficient privileges - the command references a redacted object.
merge into insert_tbl a using tibero.tb_redact_test b on ('1111112222222' = (select max(c2) from tibero.tb_redact_test)) when not matched then insert (insert_col) values (c2)
when matched then update set insert_col=c2 where a.c2 != b.c2;
delete from tibero.tb_redact_test where c1=33333;

-- USAGE12. JOIN
conn mask_user/tibero
insert into insert_tbl values('insert_tbl');
select * from (select substr(b.c2,7) from mask_user.insert_tbl a, tibero.tb_redact_test b);
-- ORA-28094: SQL construct not supported by data redaction

-- USAGE13. CASE WHEN
conn mask_user/tibero
select case when c2='1111112222222' then c2 else '0000000000000' end from tibero.tb_redact_test;
--0000000000000
select case when c2='1111112******' then c2 else '0000000000000' end from tibero.tb_redact_test;
--1111112******
select * from (select case when c2='1111112******' then c2 else '0000000000000' end from tibero.tb_redact_test);
-- ORA-28094: SQL construct not supported by data redaction

-- USAGE13-2. CASE WHEN
conn mask_user/tibero
select c2 from tibero.tb_redact_test where substr(c2,7) not in case when c2='1111112222222' then (select substr(c2,7) from tibero.tb_redact_test) else substr('0000000000000',7) end;
-- 1111112****** => BUGGGGGGGGGG
-- Tibero : result 1111112****** if '='
select c2 from tibero.tb_redact_test where substr(c2,7) not in case when c2='1111112222222' then (select * from (select substr(c2,7) from tibero.tb_redact_test)) else substr('0000000000000',7) end;
-- ORA-28094: SQL construct not supported by data redaction
select * from (select c2 from tibero.tb_redact_test where substr(c2,7) = case when c2='1111112222222' then (select substr(c2,7) from tibero.tb_redact_test) else substr('0000000000000',7) end);
-- 1111112******

-- USAGE14. FBI
conn tibero/tmax
--create index fbi_tb_redact_test2 on tibero.tb_redact_test(c1+c2); -- Tibero Error!!!!
--Index created.
conn mask_user/tibero
select c2 from tibero.tb_redact_test where c1+c2 in (select c1+c2 from tibero.tb_redact_test);
-- 1111112******
select c2 from tibero.tb_redact_test where c1+c2 = (select c1+c2 from tibero.tb_redact_test);
-- ORA-01722: invalid number (??????????)
select c2 from tibero.tb_redact_test where substr(c1+c2,7) in (select * from (select substr(c1+c2,7) from tibero.tb_redact_test));
-- 1111112******

-- USAGE15. OPERATOR (IN, =)
select to_char(c2) from tibero.tb_redact_test where '1111112222222' in (select to_char(c2) from tibero.tb_redact_test);
-- 1111112******
select to_char(c2) from tibero.tb_redact_test where '1111112222222' = (select c2 from tibero.tb_redact_test);
-- no rows selected (????????????????????)
select to_char(c2) from tibero.tb_redact_test where '1111112222222' = (select to_char(c2) from tibero.tb_redact_test);
-- no rows selected (????????????????????)
select c2 from tibero.tb_redact_test where '2222222' = (select * from (select substr(a,7) from (select to_char(c2) a from tibero.tb_redact_test)));
-- ORA-28094: SQL construct not supported by data redaction
select 1 from tibero.tb_redact_test where '2222222' = (select * from (select substr(a,7) from (select to_char(c2) a from tibero.tb_redact_test)));
-- 1
select c2 from tibero.tb_redact_test where '2222222' in (select * from (select substr(a,7) from (select to_char(c2) a from tibero.tb_redact_test)));
-- 1111112******

spool off
exit
