# ctags
* 소스 코드의 tag 파일 생성 유틸
* 생성된 tags 파일을 기반으로 특정 함수/변수의 이름으로 검색, 이동 가능
## tags파일 생성
* `ctags -R *`: 현재 디렉토리 기준 Recursive하게 모든 파일을 태깅
* `ctags -R $(find . -type f -name \*.[ch])`: .c / .h 파일만 태깅
* `ctags -R $(find . -type f -name \*.cc)`: .cc 파일만 태깅
## 설정 추가
* .vimrc
  * set tags=./tags,tags; - 현재 DIR기준으로 상위 폴더로 올라가면서 가장 가까운 tags탐색
  * set tags+=[ctags -R로 생성된 tags파일의 절대 경로]
  * set tagbsearch  - 검색속도 향상
#### tips
* 점프 + Stack Push: ctrl + ] == tj == ts == ta == tag
* 점프 + Stack Pop: ctrl + t == po
* 이전 Tag로 이동: tp
* 다음 Tag로 이동: tn
* 맨앞 Tag로 이동: tr
* 맨뒤 Tag로 이동: tl
* 수평 분할해서 점프: stj == sts
* 미리보기 창으로 점프: ptj == pts
* Stack 목록 확인: tags

