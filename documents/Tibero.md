# Tibero
* DBMS
## 사용법
### Single Tibero 사용
* tbsvr
  * 실행: `tbboot`
  * 종료: `tbdown`
  * tbsvr 재실행: `tbdown clean && tbboot`
* Client 실행: `$TB_HOME/client/bin/tbsql`
  * 줄여서 tbsql.
  * rlwrap패키지를 설치하고 .bashrc에 `alias tbsql='rlwrap tbsql'`을 추가하면 history검색 가능
* 일반 계정 로그인: `tbsql ID/PW(생략시 tibero/tmax)`
* SYS 계정 로그인: `tbsql sys/tibero`
  * 접속 후 `LS` 커맨드를 입력해 정상적으로 출력되는지 확인
### SQL 예시 정리
* 모든 User 확인
  * `SELECT * FROM ALL_USERS;`
  * `SELECT USERNAME FROM DBA_USERS;`
* SYS계정 접근(동작안함?)
  * 현재 DB_SID: `sys AS SYSDBA`
  * 다른 DB_SID: `sys@[$TB_SID] AS SYSTDBA`
* 계정 연결
  * Default계정 = tibero/tmax (ID/PW)
  * 연결: `CONN tibero/tmax;`
  * 다른 DB계정 연결: `CONN tibero/tmax@[$TB_SID]`
  * 계정 작업
    * PW수정: `ALTER USER tibero IDENTIFIED BY 'new_password'`
    * 삭제: `DROP USER tibero;`
      * SCHEMA가 엮어있는 복잡한 계정일 경우 뒤에 CASCADE를 붙여 모든 SCHEMA제거 후 삭제
* User 추가 및 설정
  * 생성: `CREATE USER user_name IDENTIFIED BY 'user_password';`
  * 권한부여: `GRANT CONNECT,RESOURCE TO user_name;`
    * 상위권한을 가진 유저로 로그인 -> 다른 유저에 권한 부여
    * 권한확인(동작안함?): `SELECT * DBA_SYS_PRIVS WHERE GRANTEE = 'CONNECT';`
    * 특정DB사용권한: `GRANT SELECT,INSERT,UPDATE,DELETE ON db_name TO user_name;`
    * DBA 사용권한: `GRANT DBA TO user_name;`
    * 권한 취소: `REVOKE CONNECT,RESOURCE FROM user_name;`
  * 계정연결: `CONN user_name/user_password;`
* Table 조작
  * 모든 테이블 보기: `LS` or `SELECT * FROM TAB;`
  * 테이블 구조 보기: `DESC table_name;`
  * 생성/수정/삭제 생략
  * Comment 추가
    * Table: `COMMENT ON TABLE table_name IS 'add comment.'`
    * Column: `COMMENT ON COLUMN table_name.column_name IS 'add comment.'`
* Tablespace 조작(DDL)
  * 생성: `CREATE TABLESPACE tablespace_name DATAFILE '[DB_PATH]/file_name.dtf' SIZE 100M EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;`
    * 그냥 `CREATE TABLESPACE tablespace_name;` 해도 됨
    * 이 경우, Default Value(100M, tablespace_name + 00N.dtf로 생성됨)
    * 하위 Datafile 생성
      * `ALTER TABLESPACE tablespace_name ADD DATAFILE 'sub_file_name.dtf' size 10M;`
      * tablespace_name이 동일한 .dtf파일이 생성됨
  * 제거: `DROP TABLESPACE tablespace_name;`
    * Tablespace 연관된 모든 데이터 지우는 방법
      * `DROP TABLESPACE tablespace_name INCLUDING CONTENTS AND DATAFILES;`
        * Contents: User, Table 등
        * Datafiles: 하위 Datafile
    * **주의**
      * 하위 컨텐츠가 없으면 DROP은 되지만 실제 파일이 제거되지 않음
      * **되도록 연관된 모든 데이터를 지우는 커맨드 사용할 것!**
  * 특정 Tablespace 사용
    * User생성시 사용할 기본 Tablespace 지정
      * `CREATE USER user_name IDENTIFIED BY 'user_pw' DEFAULT TABLESPACE tablespace_name;`
    * Table생성시 사용할 기본 Tablespace 지정
      * `CREATE TABLE table_name(c1 NUMBER) TABLESPACE tablespace_name;`
* DBA 조작(DDL)
  * DB파일경로: `$TB_HOME/database/$TB_SID` == [DB_PATH]
  * 주요 ID
    * DBA_DATA_FILES(.dtf, tablespace명=유저명/생성한Space명)
    * DBA_TEMP_FILES(.dtf, tablespace명='TEMP')
    * DBA_USERS(유저 테이블)
  * 주요 사용법
    * Files
      * 이름 확인: `SELECT TABLESPACE_NAME,FILE_NAME FROM DBA_DATA_FILES;`
      * 크기 확인: `SELECT (BYTES/1024/1024) MB FROM DBA_DATA_FILES;
      * 크기 변경: `ALTER DATABASE DATAFILE(또는 TEMPFILE) '[DB_PATH]/file_name.dtf' RESIZE 100M;`
        * ex) `ALTER DATABASE TEMPFILE '/Tibero/tibero7/database/tibero/temp001.dtf' RESIZE 1G;`
        * DB영역 수정이므로 ALTER DATABASE임에 주의!
    * Users
      * User가 속한 Tablespace  확인: `SELECT USERNAME,DEFAULT_TABLESPACE FROM DBA_USERS;`
    * _DD_ (Tibero 한정?)
      * `SELECT user_id, name FROM sys._dd_user;`
      * `SELECT name FROM sys._dd_obj WHERE name LIKE '%DD%';`
* 권한(GRANT/REVOKE)
  * 특정DB,Object 접근 권한 및 SQL 사용 권한 부여
  * 상위 권한 User가 하위 User/Role에게 권한 부여하는 방식
  * 주요 시스템 권한
    * CREATE SESSION/CREATE ROLE/ALTER USER/...
  * 주요 시스템 ROLE
    * CONNECT: 세션 생성 및 테이블 생성/조회 등의 권한
    * RESOURCE: PL/SQL을 사용하기 위해 필요한 정의된 Procedure, Trigger 사용 권한
    * **DBA: 모든 시스템 권한 부여. DB관리자에게만 부여되어야 함**
    * `SELECT * FROM DBA_ROLES;`로 확인 가능함
    * 실제 ROLE 구성정보 확인 방법
      * `SELECT grantee, privilege FROM DBA_SYS_PRIVS WHERE grantee = 'CONNECT';`
  * `GRANT [system_priviliege | role] TO [user | role | PUBLIC] [WITH ADMIN OPTION];`
    * 권한은 시스템 권한뿐만 아니라 SQL단위의 권한도 부여 가능함
    * 권한 부여 대상을 특정 유저, 특정 그룹, 전체 유저로 지정 가능
    * WITH ADMIN OPTION이 있으면 권한을 받은 User가 다른 User에게 해당 권한 부여 가능해짐
  * `GRANT [system_priviliege | role] FROM [user | role | PUBLIC];`
    * 권한 회수
    * **주의**
      * WITH ADMIN OPTION을 통해 권한을 받은 User들의 권한은 연쇄적으로 취소되지 않음
  * ROLE을 이용한 그룹 권한 부여 예
    * `CREATE ROLE role1;`
    * `GRANT CREATE SESSION, CREATE TABLE TO role1;`
    * `GRANT role1 TO user1,user2;`
* Partitioning
  * 생성 예
    * ```
      CREATE TABLE table_name (c1 NUMBER) PARTITION BY RANGE(table_name) (
      PARTITION P1 VALUES LESS THAN (10),
      PARTITION P2 VALUES LESS THAN (20));
      ```
      * -~9는 P1, 10~19는 P2와 연결된 Segment를 통해 저장됨
      * 20 이상의 값은 물리적 공간이 할당되지 않아 저장 불가
* 기타 명령어
  * DB명 확인: `SELECT NAME FROM V$DATABASE;`
  * SID명 확인(동작안함?): `SELECT INSTANCE FROM V$THREAD;`
* Lock된 계정 푸는 법
  * ERROR: The account is locked
  * Lock된 계정 확인: `SELECT USERNAME,ACCOUNT_STATUS,LOCK_DATE FROM DBA_USERS;`
    * LOCK_DATE값이 있으면 Lock걸린 계정임
  * ACCOUNT_STATUS 값에 따라 다르게 처리
    * EXPIRED & LOCKED(패스워드 기간 만료): `ALTER USER user_name IDENTIFIED BY user_pw;`
    * LOCKED(패스워드 입력 일정횟수 오류): `ALTER USER user_name ACCOUNT UNLOCK;`
* .sql 테스트
  * .sql
    * SQL언어로 작성되는 파일로, @[경로]/[sql파일명] 으로 실행할 수 있다.
      * ex) SQL> @'/Tibero/tibero7/src/tbsvr/pkg/scripts/_pkg_dbms_redact.sql'
  * Tibero 기준으로 설명
    * server의 경우 //src/tbsvr/pkg/scripts/ 경로에 .sql파일들이 위치해 있음
    * 특정 시점에 이 파일들은 빌드되어 //scripts/pkg 경로에 .tbw,.sql로 떨어져 사용됨
      * 언제 빌드되는지는 확인 필요할 듯
      * 암호화 된 파일이므로, 로컬 테스트 경로에는 부적합한 듯?
  * 로컬 테스트 순서
    * 1. .sql 수정
    * 2. `conn sys/tibero;` - 반드시 적용은 sys계정에서만 수행
    * 3. SQL> set serveroutput on; - dbms_output.put_line('contents'); 구문을 출력해 줌
    * 4. SQL> @'_pkg_dbms_redact.sql' - 경로 주는게 귀찮으면 파일위치 경로에서 conn
    * 5. sql 테스트 진행. .sql 수정시 4.부터 재실행
* Tibero Debugging
  * Trace Error 확인
    * 예시 -- tracelog
      * failed to get rp with examine. tsid=5, sgmt=889267, dba_723_01932041, rowno=41
      * Internal Error with condition 'false' (xi_exam.c:77) (pid=29523, tid=1160)
      * T7기준 sgmt_id로 바뀜
    * OBJ_ID를 찾기 위해 _dd_tbl을 sgmt_id로 검색
      * select * from sys._dd_tbl where sgmt_id='889267';
      * obj_id가 3333으로 가정
    * OBJECT_NAME을 찾기 위해 objs를 obj_id로 검색
      * select * from all_objects where object_id='3333';
      * object_name 확인
