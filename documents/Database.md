# Database
* DB의 핵심 개념, 용어 등을 정리
* 용어
  * ROWID
    * 참조 링크
      * https://m.blog.naver.com/PostView.naver?isHttpsRedirect=true&blogId=kimsajang&logNo=220507834231
    * Index생성을 위해 내부적으로 사용되는 일종의 Pseudo Column
    * 테이블이 가진 Column처럼 취급할 수 있지만, 참조만 가능
    * 실제 DB에 저장되진 않으며, 사용자가 수정 불가
    * **물리적 Addr을 가짐**, Single Block Access방식으로 [+원하는 Row에 가장 빠른 접근 가능+]
    * 출력/조건절/유일성(중복제거) 등 실제 Query조작에도 사용될 수 있음
    * ROWID표기 구조
      * AAAArs.AAD.AAAAUa.AAA
        * AAAArs(6자리): 속한 Data Object의 고유 번호
        * AAD(3자리): Relative file 번호. 속한 Datafile에 할당된 번호
        * AAAAUa(6자리): Block 번호. 속한 Data Block에 할당된 번호
        * AAA(3자리): Row 번호. Block내의 헤더에 저장된 Row Directory slot에 할당된 번호


