# cmake
* 빌드 툴인 make의 크로스플랫폼 빌드 관리 시스템
* 플랫폼에 맞는 Makefile 생성 및 관리
* 소스, 출력 바이너리를 제외한 부산물(obj 등)들을 추상화
* 실제 빌드는 make를 통해 진행됨
### Workflow
* CMakeLists.txt 작성
* configure: CMakeCache.txt 생성
* generate: CMakeFiles/, Makefile 생성
* build: make 실행하여 binary 생성
  * CMakeLists.txt 변경이 있으면 **자동으로 configure 단계부터 실행**
## 설치
* `apt install cmake`
## 사용법
* `cmake [project dir path]`
  *  CMakeLists.txt 경로 입력
* `cmake 옵션 설명
  * `-H`: Source Tree 경로 지정. configure 단계에 해당
  * `-B`: Binary Tree(gen) 경로 지정. generate 단계에 해당
  * `-D`: Define할 옵션 지정
  * `--build`: cmake를 이용해 make 실행
  * `--target`: `install`, `clean` 등의 make 타겟 지정
* 활용 예시
  * `cmake -H. -Bbuild -DCMAKE_INSTALL_PREFIX=./build/install_dir`
  * `cmake --build build --target install `
---
### Reference
* [CMake 동작 원리](https://www.tuwlab.com/ece/27234)
* [CMake 소스코드와 빌드파일 분리](https://leeminju531.tistory.com/41)
