# Linux Shell
* 사용자에게 OS기능 사용 및 서비스 구현을 위한 인터페이스를 제공하는 프로그램
  * 사용자와 OS Kernel 사이의 인터페이스 층
* 제공 타입을 CLI / GUI로 분류
## 사용법
* `#!/bin/sh`: 파일 첫머리에 작성하여 shell스크립트 파일임을 명시
## 명령어
* `pwd`
  * 현재 경로 제공
  * 절대경로를 얻으려면 `pwd -P` 사용
  * 주의: 스크립트를 읽을 때 파일을 실행한 경로를 제공함
    * ex) `~/path1/path2/file.sh`를 실행할 때
      * `~/` 경로에서 file.sh를 실행할 경우, pwd는 `~/`이 됨
      * `~/path1/` 경로에서 file.sh를 실행할 경우, pwd는 `~/path1`이 됨
    * file.sh의 경로를 얻고 싶다면 다음과 같이 명령어 수행
      * `DIR="$( cd "$( dirname "$0" )" && pwd -P )"`
* `dirname [sh 파일명 경로]`
  * 파일명을 제외한 경로를 리턴
* `echo`
  * 텍스트 출력 제공
* `$0`
  * Shell명령어의 args를 의미
  * 스크립트 파일을 다음과 같이 실행 `sh example.sh arg1 arg2`
    * 이 때, $0은 `example.sh`, $1은 `arg1`, $2는 `arg2`가 됨
* `source`
  * sh스크립트 실행
  * 주의: source의 경우 파일값(`$0`)이 리턴되지 않음
    * `$0` 대신 `$BASH_SOURCE`로 sh 경로값을 얻을 수 있음
* Reference
  * [Shell스크립트 절대경로 얻기](https://codechacha.com/ko/how-to-get-path-of-bash-script/)
