#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import sys
import optparse
import errno

# Const set ----------------------------------------

CWD_DIR = os.path.normpath(os.getcwd())
CURRENT_DIR = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "."))
VAR_FILE= os.path.normpath(os.path.join(CURRENT_DIR, "build_variables.py"))

#/*------------- Start of BuildManager ------------#

class BuildManager:
  def __init__(self, options):
    self.options = options
    self.init_from_build_variables_file();

  def init_from_build_variables_file(self):
    if self.options.init:
      print("[ Init ]")
      if not os.path.isfile(VAR_FILE + ".eg"):
        print("Error: Can't find %s.eg." % (VAR_FILE) )
        sys.exit(-1)
      elif not os.path.isfile(VAR_FILE):
        print("Copying from %s.in ..." % (VAR_FILE))
        self.sh("cp -f", VAR_FILE + ".in", VAR_FILE)
      else:
        print("Warning: build_variables.py is already exist.")
    elif not os.path.isfile(VAR_FILE):
      print("Error: Can't find %s." % (VAR_FILE))
      print("  Require %s.\n  It should be copied from %s.in." % (VAR_FILE, VAR_FILE))
      print("\n  Run `./init_config.py --init` for copy.\n")
      sys.exit(-1)

    import build_variables

  def all(self):
    print("[ call all ]")
    self.init_settings()
    self.configure()

  def init_settings(self):
    print("[ call init_settings ]")

  def configure(self):
    print("[ call configure ]")

# Utils --------------------------------------------
  # Shell Command Without Fail
  def shWOF(self, *commands):
    command = ' '.join(commands)
    return os.system(command)

  # Shell Command
  def sh(self, *commands):
    ret = self.shWOF(*commands)
    if ret:
      sys.exit(1)

# CommandLine args ---------------------------------
  @staticmethod
  def parse_args(parser, argv):
    parser.add_option('--init', action='store_true',
                      help='Create build_variables.py from .in file.')
    parser.add_option('--all', action='store_true',
                      help='Execute all options.')
    parser.add_option('-i', '--initial-settings', action='store_true',
                      help='Set default environment.')
    parser.add_option('-c', '--configure', action='store_true',
                      help='Configure')
    parser.add_option('-t', '--target', type='string', default='target_name',
                      help='Specify target for build.')

    return parser.parse_args(argv)

#-------------- End of BuildManager -------------*/#


# Main for init_config.py --------------------------
def main(argv):
  parser = optparse.OptionParser(description=sys.modules[__name__].__doc__)
  options, args = BuildManager.parse_args(parser, argv)

  if not argv:
    print("Error: Require one or more options.\n")
    parser.print_help()
    return errno.EINVAL
  if args:
    parser.error('Unrecognized command line arguments: %s.' % ', '.join(args))
    return errno.EINVAL

  bm = BuildManager(options)
  if options.all:
    bm.all()
  else:
    if options.initial_settings:
      bm.initial_settings()
    if options.configure:
      bm.configure()
  print("Success!")

if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
