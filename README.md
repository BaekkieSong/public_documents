# Sbkdocuments
## Summary
작업 기록 문서
## Related pages
* 내 [Profile](https://gitlab.com/BaekkieSong/BaekkieSong)
* 작업 내용 정리 - https://gitlab.com/BaekkieSong/documents/-/milestones
* 코드 연습장 - https://gitlab.com/BaekkieSong/FP_TEST
## Gitlab web test page
* https://baekkiesong.gitlab.io/public_documents
