// algorithm
#include <string>

#include <iostream>
#include <cassert>

namespace custom {
/* all_of
범위 내의 모든 원소가 단일연산 조건을 만족하는지 판단
입력: Iterator(first), Iterator(last), UnaryPredicate
출력: bool
*/
template <typename Iter, typename UnaryPred>
bool all_of(Iter first, Iter last, UnaryPred func) {
  while (first != last) {
    if (!func(*first))
      return false;
    else
      ++first;
  }
  return true;
}

/* any_of
범위 내의 모든 원소 중 하나라도 단일연산 조건을 만족하는지 판단
입력: Iterator(first), Iterator(last), UnaryPredicate
출력: bool
*/
template <typename Iter, typename UnaryPred>
auto any_of(Iter first, Iter last, UnaryPred func) {
  while (first != last)
    if (func(*first))
      return true;
    else
      ++first;
  return false;
}

/* none_of
범위 내의 모든 원소가 단일연산 조건을 만족하지 않는지 판단
입력: Iterator(first), Iterator(last), UnaryPredicate
출력: bool
*/
template <typename Iter, typename UnaryPred>
bool none_of(Iter first, Iter last, UnaryPred func) {
  while (first != last)
    if (func(*first))
      return false;
    else
      ++first;
  return true;
}

/* find_if
조건을 만족하는 첫번째 값의 Iterator 반환
입력: Iterator(first), Iterator(last), UnaryPredicate
출력: Iterator
*/
template <typename Iter, typename UnaryPred>
Iter find_if(Iter first, Iter last, UnaryPred func) {
  while (first != last) {
    if (func(*first))
      return first;
    else
      ++first;
  }
  assert(first == last);
  return first;
}

/* for_each
Param: Iterlator(first), Iterator(last), UnaryPredicate
Return: UnaryPredicate
*/
template <typename Iter, typename UnaryPred>
UnaryPred for_each(Iter first, Iter last, UnaryPred func) {
  while (first != last) {
    func(*first);
    ++first;
  }
  return func;
}
/* for_each_n
Param: Iterlator(first), Size, UnaryFunction
Return: Iterator (== std::advance(first, n))
*/
template <typename Iter, typename Size, typename UnaryFunc>
Iter for_each_n(Iter first, Size n, UnaryFunc func) {
  for (int i=0; i < n; ++i) {
    func(*first);
    ++first;
  }
  return first;
}



} // namespace custom
