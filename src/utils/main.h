template <typename Container>
void PrintExp(Container* cont) {
  for (auto* it : *cont) {
    it->AddPrintInfo();
  }
}

template<typename EXPR>
void PrintExpr(EXPR expr) {
}


//item41 Template. implicit interface + compile time polymorphism
class W41 {
  public:
    virtual int getData() { return w_; }
    void print() { std::cout << __LINE__ << ", " << __func__ << std::endl;}
  private:
    int w_ = 41;
};
template<typename T>
void doProcess(T& data) {
  std::cout << __LINE__ << ", " << data.getData() << std::endl; // 컴파일타임 다형성.
  data.print(); //암시적 인터페이스. T::print가 있어야 함
//  data.a(); // Error. class W41 has no W41::a.
}
//template<>
//void doProcess(W41& w) {
//  std::cout << __LINE__ << ", " << w.getData() << std::endl; // 런타임 다형성.
//  w.print();  //명시적 인터페이스.
//}

//item42 usage typename keyword
  // 상속클래스리스트, 초기화리스트에 있는 의존타입네임에는 typename키워드 사용X
template<class T> //==template<typename T>
void printData(T& data) {
  // 템플릿매개변수T에 종속된 의존타입네임을 가지므로 구문분석 오류 발생.
  // T가 가진 const_iterator라는 멤버로 취급될 수도 있음.
  // 현재테스트 경우, typename 키워드 없으면 non type으로 파싱됨
  typename T::const_iterator iter(data.begin()); //Error. T is a dependent scope.
  while(iter != data.end()) {
    std::cout << *iter++;
  }
  std::cout << std::endl;

}
template<typename T>
void widgetType(T t) {
  typedef typename std::iterator_traits<T>::value_type value_type;
  value_type temp = *t;
  std::cout << temp << std::endl;
}


//item43
class MsgInfo{};
template<typename Company>
class MsgSender
{
  public:
    void sendClear(const MsgInfo& info)
    {
      std::string msg;
      // info 로부터 msg 를 생성
      Company c;
      c.sendCleartext(msg);
    }
};
class XXX43 { // 얘는 가능
  public:
    void sendClear(const MsgInfo& info) {};
};
template<typename Company>
class LogMsgSender : public MsgSender<Company> // XXX43은 가능
{
  public:
    using MsgSender<Company>::sendClear; // 얘때문에 sendClear(info)동작
    void sendLogClear(const MsgInfo& info)
    {
//      sendClear(info); //Error. LogMsgSender구문분석 중 MsgSender라는 Base템플릿 분석 불가(아직 어디서 파생되었는지 모르는 상태)
      // Company가 정확히 뭔지 모르기 때문에 MsgSender<Company>가 뭔지도 알수 없음

      //해결. sendClear가 어디서 왔는지를 알려줌
      //1. this 2. using MsgSender<Company>::sendClear 3. using처럼 명시
      this->sendClear(info);
      MsgSender<Company>::sendClear(info);
      sendClear(info);
    }

};

//item44 템플릿비대화 방지
// 템플릿 매개변수 비종속 코드는 빼내자.
// 템플릿 매개변수 -> 함수 매개변수,클래스 멤버로 바꿔서 생성량 줄이자.
// 타입 매개변수 -> 공유 함수 구현

template<typename T, std::size_t n> //주의. n값만 달라도 템플릿 2벌생성
class SquareMatrix
{
  public:
//    SquareMatrix(T a, std::size_t b) : a_(a), b_(b){}
     void invert() { std::cout << __func__<<std::endl;}
  private:
     T a_;
     std::size_t b_;
};
//해결1 invert를 Base<T>클래스 메서드로 바꾸면 invert한번만 생성됨
//but, 그냥 놔두는게 최적화 여지도 있고, Base에대한 포인터로 인한 크기 증가 문제등도 있을 수 있음 -> 상황따라 적용

//item45
//호환되는 모든 타입을 처리할 때는 메서드 템플릿화
//복사생성자/대입연산자를 템플릿으로! smartptr템플릿 구현시 get()을 잘 활용하면 암시적 변환 허용되는 선에서 동작하게끔 할 수 있다 -> 잘못된 업캐스팅/타입캐스팅 방지
//item46
//template<typename T>
//const Rational<T> operator*(const Rational<T>& lhs, const Rational<T>& rhs)
//같은 암시적타입변환을 허용하고 싶다면 friend 활용
/*
   friend
   const Rational operator*(const Rational& lhs, const Rational& rhs)
   {
   return Rational(lhs.numerator() * rhs.numerator(),
   lhs.denominator() * rhs.denominator());
   }
   위와 같이 템플릿클래스에 friend operator를 정의하면
   템플릿 인자 추론 과정에 영향을 받지 않는다!(일종의 일반 함수 선언)
   -> 인자 추론 과정은 함수 템플릿에만 적용되고, 클래스 템플릿에 영향X
   -> 함수 템플릿인자 추론 과정에선 암시적타입변환이 아예 고려되지 않기 때문에, 이러한 명시가 없으면 오류 발생.
 */
// friend 사용이유? 클래스 내에 비멤버함수 선언하는 유일한 방법. 모든인자에 대한 암시적 타입변환하기 위해 클래스내 선언 필요
//주의: 클래스내에 정의된 함수는 암시적 인라인화 되기 때문에 최대한 구현부는 밖으로 빼내자
