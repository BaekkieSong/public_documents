#include<iostream>
#include<vector>

namespace widget {
class WidgetImpl
{
  private:
    int a, b, c;
    std::vector<double> v;
};

class Widget
{
  public:
    Widget(WidgetImpl* impl) : pImpl(impl) {
    std::cout << __func__ <<std::endl;
}
    Widget(const Widget& rhs) {
    std::cout << __func__ <<std::endl;
      this->pImpl = rhs.pImpl;
    }
    // Widget 복사를 위해 포인터를 복사
    Widget& operator=(const Widget& rhs)
    {
    std::cout << __func__ <<std::endl;
      *pImpl = *(rhs.pImpl);
      return *this;
    }

    void swap(Widget& b) {
      using std::swap;
      swap(pImpl, b.pImpl);
    }
  private:
    // Widget 의 실제 데이터를 가진 객체에 대한 포인터
    WidgetImpl *pImpl;
};

void swap(Widget& a, Widget& b)
{
  a.swap(b);
//  swap(a.pImpl, b.pImpl);
}

int main() {
  auto widget_impl = new WidgetImpl;
  auto widget = new Widget(widget_impl);
  Widget widget2 = *widget;
  Widget widget3(new WidgetImpl);
  widget3 = *widget;

  return 0;
}
}
