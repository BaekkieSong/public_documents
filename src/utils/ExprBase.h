class ExpBase {
  public:
    ExpBase();
    virtual ~ExpBase();
    virtual void AddPrintInfo() = 0;
};

