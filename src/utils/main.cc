#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <functional>
#include <sstream>
#include <iomanip>

#include "boost_test.h"
#include "type_traits.h"

#include "ExprOp.h"
#include "main.h"
#include <memory>

using std::cout;
using std::endl;


//item52 커스텀 operator 구현시 new/delete쌍쌍바 구현, new가리기 방지
/*
 * new(size_t, std::ostream& log) throw(std::bad_alloc)
 * <=> delete(size_t, std::ostream& log) throw() 구현해주기
 * 클래스 안에서 new(size_t, std::ostream& log)를 선언했다면 다른 new연산자 모두 가려짐
 * ex) new(size_t)
 * ex2) Base/Derived클래스 각각 new 구현시 Base의 new는 가려짐
 * new/delete를 전문구현한 클래스 만들고 Base에서 상속받게 하기 + 확장 new/delete 구현
 */
//item51 new/delete 구현 관례
/* new
 * 1. size 0에 대한 예외처리 min=1
 * 2. size에 맞는 메모리할당 반복시도
 * 3. 할당성공시 포인터 반환
 * 4. 할당실패시 new_handler 정의
 * 5. 클래스 등 예상 초과하는 메모리블록할당 요구 처리
 * delete
 * 1. delete대상이 nullptr -> 아무것도 안함
 */
//item50 new/delete 언제 바꾸나
/*
 * 1. 테스트(탐지용 바이트, 메모리 할당 통계수집)
 * 2. std memory fragmentation 효율 개선
 * 3. 공간 단위 메모리 관리
 * 4. 원하는 단위의 메모리 할당/해제 보장 -> 할당 효율성 up
 */
//item49 std::new_handler
/* new_handler 고려사항
 * 1. 메모리 확보(상속클래스 할당시 Base보다 큰 size -> ::operator new(size)리턴해 상속클래스size처리)
 * 2. new_handler내에서 다른 new_handler를 설치, 제거(set_new_handler(0))한다.
 * 3. 예외를 Throw하거나 Abort/Exit처리해준다.
 */
void OutOfMem49()
{
    std::cerr << "Unable to satisfy request for memory\n";
    std::abort(); //없으면 cerr계속뜸
}
class Widget49 {
  public:
    static std::new_handler set_new_handler(std::new_handler nh) throw();
    static void* operator new(std::size_t size) ;//throw(std::bad_alloc);
  private:
    static std::new_handler widget_new_handler_;
    int i_;
    int j_;
};
std::new_handler Widget49::widget_new_handler_ = 0;
std::new_handler Widget49::set_new_handler(std::new_handler nh) throw() {
  std::new_handler old = widget_new_handler_;
  widget_new_handler_ = nh;
  return old;
}
class Handler49 {
  public:
  Handler49(std::new_handler nh) : new_handler_(nh) {}
~Handler49() { std::set_new_handler(new_handler_);}
  private:
  Handler49(const Handler49&);
  Handler49& operator=(const Handler49&);

  std::new_handler new_handler_;
};

// new에 들어오는 size는 멤버전체사이즈. 기본: size 1(0->최소값 1로처리), int멤버 2개: size 8
void* Widget49::operator new(std::size_t size) {//throw(std::bad_alloc) {<-IOS C++1z does not allow dynamic exception specifications
  Handler49 h(std::set_new_handler(widget_new_handler_));
  return ::operator new(size);
}
//item48 템플릿 장점
/* 1. 컴파일 타임 체크,런타임 효율화
 * 2. 연산 효율화 (루프 최적화 등)
 * 3. 맞춤식 디자인 패턴, gen방식의 프로그래밍
 */
//item47 타입정보는 traits활용
/*
 * 1. 활용법클래스내에 typedef로 태그 카테고리 지정
 * 2. traits 지정 template struct traits { typedef typename IterT::category category; }
 * 3. 타입별 함수 템플릿 구현
 * 4. 함수 호출시 category태그를 인자로 같이 전달하여 원하는 category함수 호출
 */

//item40
class base1_40 {
  int i;
};
class base2_40 {
  int i;
};
class child_40 : public base1_40, public base2_40 {
  int x;
};

//item39
//private상속 = 인터페이스 제공 X. private멤버 접근, override만. ~= 합성으로 사용하기.
// 장점? EBO(Empty Base Optimization)
class Person39 {};
class Student39: private Person39 {};

void eat39(const Person39& p) {}
void study39(const Student39& s) {}

//item37
class Shape37
{
  public:
      enum ShapeColor { Red, Green, Blue };

        // 모든 도형은 자기 자신을 그리는 함수를 제공해야 함
        virtual void draw(ShapeColor color = Red) const = 0;
};

class Rectangle37: public Shape37
{
  public:
      // 기본 매개변수 값이 달라짐!
      virtual void draw(ShapeColor color = Green) const { std::cout << "Color: " << color << std::endl;}
};

class Circle37: public Shape37
{
  public:
      virtual void draw(ShapeColor color) const { std::cout << "Color: " << color << std::endl;}
};
//item36
class B36
{
  public:
    void mf() {std::cout << "B36::mf" << std::endl;}
};

class D36: public B36
{
  public:
    // B36::mf 를 가린다
    void mf() {std::cout << "D36::mf" << std::endl;}
};
//item33
class Base33
{
  private:
    int x;
  public:
    virtual void mf1() = 0;
    virtual void mf1(int) {cout << __func__ << ", " << __LINE__ << endl;}
};
void Base33::mf1() {}

class Derived33: private Base33
{
  public:
    // 암시적으로 인라인 함수가 됨
    virtual void mf1()
    {
      Base33::mf1();
    }
};
//item13
std::shared_ptr<int[]> sp(new int[10]);
std::shared_ptr<int> single(new int[10],std::default_delete<int>());
std::unique_ptr<int> ui(new int);

//item12
class Expr {
  public:
    virtual void print() { std::cout << __func__ << __LINE__ << std::endl;}
    Expr(Expr& expr) = default;
    Expr(char* s, int bi) {
      s_ = new std::string(s);
      bi_ = bi;
    }
    Expr& operator=(Expr& expr) {
      std::string* temp_s = s_;
      s_ = new std::string(expr.s_->c_str());
      delete temp_s;
    }
//    virtual ~Expr() { std::cout << __func__ << ", s: " << *s_ << ", bi: " << bi_ << std::endl;}
    std::string* s_;
    int bi_;
  private:
};
class WrapExpr : public Expr {
  public:
    WrapExpr(Expr& expr,int i, int bi) : Expr(expr), i_(i) {
      std::cout << this->s_->c_str() << std::endl;
      std::cout << this->i_ << std::endl;
      bi_ = bi;
      std::cout << this->bi_ << std::endl;}
    void print() override { std::cout << __func__ << __LINE__<<std::endl;}
  int i_;
};

//typedef int IntArray[5];
using IntArray = int[5];

class FontHandle {
  public:
    void Print() { std::cout << font_ << std::endl;}
  private:
    std::string font_ = "fonthandlestring";
};
class Font {
  public:
    Font(FontHandle font_handle) : font_handle_(font_handle) {}
    operator FontHandle() const { return font_handle_; }
    operator int() const { return 1; }
    Font& operator =(Font& font) { return *this; }
  private:
    FontHandle font_handle_;
};

class B {
 public:
   virtual void Print() { std::cout << "BBBBB" << std::endl; }
};

class C : public B {
 public:
   void Print() { std::cout << "CCCCC" << std::endl; }
};

class NamedObject {
  private:
    std::string name_;
    int age_;
  public:
    NamedObject(std::string name, int age) : name_(name), age_(age) {
    }
    void dog_info_print();
};
void NamedObject::dog_info_print() {
  std::cout << name_ << std::endl;
}



class Initialize {
  public:
Initialize(int x,  std::string z, int y) : z_(z), x_(x),y_(y) {
   std::cout << "X: " << x_ << ", Y: " << y_ << ", Z: " << z_ << std::endl;
}
  private:
int x_;
int y_;
std::string z_;
};
class Functor {
  public:
int& func() {
  return const_cast<int&>(static_cast<const int&>(value));
}
private:
const int value=3;
};

class CTextBlock
{
    public:
      CTextBlock(char* text) : pText(text) { }
          // 부적절한 (그러나 비트수준 상수성이 있어 허용되는) operator[] 의 선언
          char& operator[](std::size_t position) const
                { return pText[position]; }
    private:
        char *pText;
};

void printStaticData() {
  static int i=0;
  std::cout << "Call this func: " << ++i << std::endl;
}

namespace custom {
template <class T>
void swap(T& a, T& b) {
  T tmp = std::move(b);
  b = std::move(a);
  a = std::move(tmp);
}
template <class Iter, class Func>
void sort(Iter first, Iter last, Func func) {
  for (Iter i = last; i > first; --i) {
    for (Iter j = first; j < i-1; ++j) {
      if (func(*j, *(j+1))) {
      } else {
        std::iter_swap(j, j+1);
      }
    }
  }
}
template <class T = void>
struct less {
  bool operator() (T a, T b) { return (a<=b) ? true : false; }
};
template <class T = void>
struct greater {
  bool operator() (T a, T b) {return (a >= b) ? true : false; }
};
}

#define PRETTY false
#define FUNC_NAME (PRETTY) ? __PRETTY_FUNCTION__ : __func__
void InitializeTest(const std::string& func_name) {
  std::cout << "[" << func_name << "]" << std::endl;
}
void EndTest() {
  std::cout << "----------------------------" << std::endl;
}

// 조건에 따라 원소 정렬
template <class T, class Func>
T& GetSortedElements(T& elements, Func func = std::greater<>()) {
  std::sort(elements.begin(), elements.end(), func/*std::less<>() or std::greater<>(), ...*/);
  return elements;
}

template <class T>
std::vector<T>& GetUniqueElements(std::vector<T>& elements) {
  // std::unique는 '인접하면서 같은 값'을 가진 원소를 리스트 맨 뒤로 옮김(삭제X)
  auto it =
      std::unique(elements.begin(), elements.end(),
          [](T& pre, T& cur) {
              return (pre == cur) ? true : false;
          });
  // it은 옮겨진 원소의 첫번째 위치이며, erase를 통해 원소 제거 필요
  elements.erase(it, elements.end());
  return elements;
}

std::pair<std::string, std::string> SplitStringBySpace(std::string s) {
  std::stringstream ss(s);
  std::string first, second;
  ss >> first >> second;
  return std::make_pair<std::string, std::string>((std::string)first, (std::string)second);
}

// condition을 기준으로 하나의 string을 여러 string으로 분할해 반환
std::vector<std::string> SplitStrings(std::string& s, char&& condition) {
  std::stringstream ss(s);
  std::vector<std::string> str_vec;
  std::string temp_str;
  while(std::getline(ss, temp_str, condition)) {
    str_vec.push_back(temp_str);
  }
  return str_vec;
}

void Test_UniqueSort() {
  InitializeTest(FUNC_NAME);
  std::vector<std::string> v = { "a", "c", "z", "b", "d", "b", "b", "c", "h", "h", "z" };
  auto& sorted_v = GetSortedElements(v, std::less<>());
  cout << &v << ", " << &sorted_v << endl;
  std::cout << "Only sorted: ";
  for (auto s : sorted_v)
    cout << s << ", ";
  std::cout << std::endl;
  // unique는 인접 중복만 해결되므로, sort 후에 unique를 해야 모든 중복 원소 제거 가능함
  /* auto& uniqued_v = */GetUniqueElements(v);
  std::cout << "Unique sorted: ";
  for (auto s : v)
    cout << s << ", ";
  cout << endl;
  EndTest();
}

void Test_SplitString() {
  InitializeTest(FUNC_NAME);
  std::string pre_pair_split_str = "aaaaaa bbbbbb";
  std::cout << "split the string: " << pre_pair_split_str << std::endl;
  auto pair_split_strs = SplitStringBySpace(pre_pair_split_str);
  cout << pair_split_strs.first << ", " << pair_split_strs.second << endl;

  std::string pre_split_strs = "a bb ccc dddd";
  std::cout << "split the string: " << pre_split_strs << std::endl;
  auto split_strs = SplitStrings(pre_split_strs, ' ');
  for (auto s : split_strs)
    cout << s << ", ";
  cout << endl;
  EndTest();
}

#include "algorithm_impl.h"
#include <functional>

typedef std::function<bool(std::string)> UnaryPredicate;
UnaryPredicate unary_pred = [](std::string str){ return str.size() == 3 ? true : false; };

void Test_AllOf() {
  InitializeTest(FUNC_NAME);
  std::vector<std::string> str_vec = { "aaa", "bbb", "ccc" };
  bool result =
    custom::all_of(str_vec.cbegin(), str_vec.cend(), unary_pred);
  cout << "all_of result(success): " << result << endl;
  str_vec = { "aaa", "bbb", "cc" };
  result =
    custom::all_of(str_vec.cbegin(), str_vec.cend(), unary_pred);
  cout << "all_of result(error): " << result << endl;

  result = all_of(str_vec.cbegin(), str_vec.cend(), unary_pred);
  cout << "all_of result(error): " << result << endl;
  EndTest();
}

void Test_AnyOf() {
  InitializeTest(FUNC_NAME);
  std::vector<std::string> str_vec = { "aa", "b", "ccc" };
  bool result =
    custom::any_of(str_vec.cbegin(), str_vec.cend(), unary_pred);
  cout << "any_of result(success): " << result << endl;
  str_vec = { "a", "b", "cc" };
  result =
    custom::any_of(str_vec.cbegin(), str_vec.cend(), unary_pred);
  cout << "any_of result(error): " << result << endl;
  EndTest();
}

void Test_NoneOf() {
  InitializeTest(FUNC_NAME);
  std::vector<std::string> str_vec = { "aa", "b", "ccc" };
  bool result =
    custom::none_of(str_vec.cbegin(), str_vec.cend(), unary_pred);
  cout << "none_of result(error): " << result << endl;
  str_vec = { "a", "b", "cc" };
  result =
    custom::none_of(str_vec.cbegin(), str_vec.cend(), unary_pred);
  cout << "none_of result(success): " << result << endl;
}

void Test_FindIf() {
  InitializeTest(FUNC_NAME);
  // TODO - cbegin, cend 쓰면 에러나는 이유?
  std::vector<std::string> str_vec = { "aa", "b", "ccc" };
  std::vector<std::string>::iterator result =
    custom::find_if(str_vec.begin(), str_vec.end(), unary_pred);
  cout << "find_if result(success): " << *result << endl;
  str_vec = { "a", "b", "cc" };
  result =
    custom::find_if(str_vec.begin(), str_vec.end(), unary_pred);
  cout << "find_if result(error): " << (result != str_vec.end()) << endl;
  EndTest();
}

struct Sum {
  void operator()(int n) { sum += n; };
  int sum = 0;
};
void Test_ForEach() {
  InitializeTest(FUNC_NAME);
  std::vector<int> nums{3, 4, 2, 8, 15, 267};
  auto print = [](const int& n) { std::cout << " " << n; };
  std::cout << "before: ";
  custom::for_each(nums.cbegin(), nums.cend(), print);
  std::cout << std::endl;
  std::cout << "after: ";
  custom::for_each(nums.begin(), nums.end(), [](int& n){++n;});
  custom::for_each(nums.cbegin(), nums.cend(), print);
  std::cout << std::endl;
  Sum s = custom::for_each(nums.cbegin(), nums.cend(), Sum());  // Create Sum object(Functor).
  std::cout << "Sum result: " << s.sum << std::endl;
  int a[]{1,2,3,4,5};
  auto x = std::begin(a);
#include <typeinfo>
  std::cout << typeid(x).name() << std::endl;
//  custom::for_each(x.begin(), x.end(), print);
  custom::for_each(a, a+5, print);
  std::cout << std::endl;
  EndTest();
}

void Test_ForEachN() {
  InitializeTest(FUNC_NAME);
  std::vector<int> nums{3, 4, 2, 8, 15, 267};
  std::vector<int>::iterator it =
      custom::for_each_n(nums.begin()+1, 4, [](int i) {std::cout << i << ", "; });
  std::cout << "last: " << *it << std::endl;
  EndTest();
}

void Test_CustomSort() {
  InitializeTest(FUNC_NAME);
  std::vector<int> int_v{4,1,5,7,8,3};
  std::cout << "compare 4, 1 using custom::less & std::less" << std::endl;
  std::cout << std::boolalpha << "custom::less : " << custom::less<int>{}(int_v[0], int_v[1]) << std::endl;
  std::cout << std::boolalpha << "std::less : " << std::less<>{}(int_v[0], int_v[1]) << std::endl;
  for (auto i : int_v)
    std::cout << i << ", ";
  std::cout << std::endl << std::left << std::setw(10) << "less: ";
  custom::sort(int_v.begin(), int_v.end(), custom::less<int>());
  for (auto i : int_v)
    std::cout << i << ", ";
  std::cout << std::endl << std::setw(10) << "greater: ";
  custom::sort(int_v.begin(), int_v.end(), custom::greater<int>());
  for (auto i : int_v)
    std::cout << i << ", ";
  std::cout << std::endl;
  EndTest();
}

void Test_StringUtils() {
  std::string text = "text";  // text
  std::string text_added = text + "added";  // textadded
  std::string added = text_added.substr(text.size(), std::string::npos);  // added
  assert(text + added == text_added);

  std::string copied_text("copied");
  std::copy(text.begin(), text.end(), std::back_inserter(copied_text)); // copiedtext
  assert("copied" + text == copied_text);
  size_t pos = copied_text.find("text");  // pos == 6
  assert(pos == copied_text.size()-text.size());  // copied_text size: 6, text size: 4
}

class NodeIterator;
class Node {
  public:

//template <typename T>
class NodeIterator {
 public:
     NodeIterator() {
       this->node_ = NULL;
     }

     NodeIterator(const NodeIterator & it) {
       this->node_ = it.node_;
       this->index = it.index;
     }

     NodeIterator(Node *node_, int index) {
       this->node_ = node_;
       this->index = index;
     }

     NodeIterator & operator++() {
       index++;
       return *this;
     }

     NodeIterator operator++(int) {
       NodeIterator copy(*this);
       operator++();
       return copy;
     }

     NodeIterator & operator--() {
       index--;
       return *this;
     }

     NodeIterator operator--(int) {
       NodeIterator copy(*this);
       operator--();
       return copy;
     }

     bool operator==(const NodeIterator & rhs) {
       return node_ == rhs.node_ && index == rhs.index;
     }

     bool operator!=(const NodeIterator & rhs) {
       return !(*this == rhs);
     }

     bool operator<(const NodeIterator & rhs) {
       extern void error(std::string msg);
       if (node_ != rhs.node_) error("NodeIterators are in different vectors");
       return index < rhs.index;
     }

     bool operator<=(const NodeIterator & rhs) {
       extern void error(std::string msg);
       if (node_ != rhs.node_) error("NodeIterators are in different vectors");
       return index <= rhs.index;
     }

     bool operator>(const NodeIterator & rhs) {
       extern void error(std::string msg);
       if (node_ != rhs.node_) error("NodeIterators are in different vectors");
       return index > rhs.index;
     }

     bool operator>=(const NodeIterator & rhs) {
       extern void error(std::string msg);
       if (node_ != rhs.node_) error("NodeIterators are in different vectors");
       return index >= rhs.index;
     }

     NodeIterator operator+(const int & rhs) {
       return NodeIterator(node_, index + rhs);
     }

     NodeIterator operator+=(const int & rhs) {
       index += rhs;
       return *this;
     }

     NodeIterator operator-(const int & rhs) {
       return NodeIterator(node_, index - rhs);
     }

     NodeIterator operator-=(const int & rhs) {
       index -= rhs;
       return *this;
     }

     int operator-(const NodeIterator & rhs) {
       extern void error(std::string msg);
       if (node_ != rhs.node_) error("NodeIterators are in different vectors");
       return index - rhs.index;
     }

     Node& operator*() {
       return *(node_->node_);
     }

     Node* operator->() {
       return (node_->node_);
     }

     Node& operator[](int k) {
       return *(node_->node_); // +k
     }

   private:
    Node *node_;
    int index;
  };
  public:
  virtual void print() { std::cout << "Node" << data <<  std::endl;}
  virtual void next() {
    data -= 1;
    if (node_)
      node_->next();
    Node::print();
    print();
  }
  void add(Node* node) {
    node_ = node;
  }
  int data = -1;
  Node* node_ = nullptr;

  NodeIterator begin() {
    return NodeIterator(this, 0);
  }
  NodeIterator end() {
    return NodeIterator(this, -1);
  }
};
class ANode : public Node {
  public:
  void print() override { std::cout << "A: " << a << std::endl;}
  void next() override {
    ++a;
    Node::next();
  }
  int a=0;
};
class BNode : public Node {
  public:
  void print() override { std::cout << "B: " << b << std::endl;}
  void next() override {
    ++b;
    Node::next();
  }
  int b=100;
};

#include <bitset>
class PrintFlag {
 public:
  enum Flag {
    USE_PRINT_LPN = 0,
    USE_PARSER_PRINT,
    USE_OPT_PRINT,
    LAST
  };
  void Reset() { flags_.reset(); }
  void SetAll() { flags_.set(); }
  void Set(Flag&& flag) { flags_.set(flag, true); }
  void UnSet(Flag&& flag) { flags_.set(flag, false); }
  bool IsSet(Flag&& flag) { return flags_[flag]; }
  std::string GetFlags() { return flags_.to_string(); }
 private:
  std::bitset<6> flags_;
};

class Base {
  public:
  virtual std::string Print(int i) {return "Base";}
  std::string Print() {
    return "XXXXXX";
  }
  void X() { std::cout << "X" << std::endl; }
};

class Expn : public Base{
  public:
    virtual std::string Print(int i) override {
      return "Expn";
    }
    std::string Print() {
      std::cout << "ExpnPrint  ";
      return Print(1);
    }

};

int g_level = 2;
bool ShouldCreateLogMessage(int severity) {
  if (severity < g_level)
    return false;
  return severity >= g_level;
}

class Message {
  public:
  Message(const char* file, int severity): severity_(severity) {
    Init(file);
  }
  ~Message() {
    stream_ << std::endl;
    std::string print_line(stream_.str());
    fwrite(print_line.data(), print_line.size(), 1, stderr);
    fflush(stderr);
  }
  std::ostream& stream() { return stream_; }
  int severity() { return severity_; }
  std::string str() { return stream_.str(); }

  std::string operator<< (std::string sub) {
    return sub;
  }
  private:
    void Init(const char* file) {
      if (severity_ >= 0)
        stream_ << "Severity[" << severity_ << "]";
      stream_ << ": ";
      message_start_ = stream_.str().length();
    }
    int severity_;
    std::stringstream stream_;
    size_t message_start_;
};
class MessageVoidify {
  public:
    MessageVoidify() = default;
    void operator & (std::ostream&){}
};
class Stream {
  public:
    Stream() = default;
    template <typename T>
    std::string str() {
      return stream_.str();
    }
  private:
    std::stringstream stream_;
};
class STRLOG : std::streambuf {
  public:
    STRLOG() = default;
    STRLOG(std::string& str) : str_(str) {}

    std::string str() const {
      //return oss_->str();
      return str_;
    }
    STRLOG& operator<<(std::string data) {
      //*oss_ << data;
      str_ += data;
      return *this;
    }
//    template <typename T>
//    std::ostream& operator<<(T& data) {
//      oss_ << data;
//      return stream_;
//    }
  private:
    std::ostringstream* oss_;
    std::string& str_;
};
class GSTRLOG : std::streambuf {
  public:
   GSTRLOG() = default;
   std::string str() const {
     return ss_.str();
   }
   template<typename T>
   GSTRLOG& operator<<(const T& data) {
     ss_ << data;
     if (flag_) {
       ss_ << "\n";
       flag_ = false;
     }
     return *this;
   }
   void set_line() {
    flag_ = true;
   }
  private:
   std::stringstream ss_;
   bool flag_ = false;
};
class Voidify{
  public:
    Voidify() = default;
    void operator & (GSTRLOG&){}
};

#define ON(severity) ShouldCreateLogMessage(severity)
#define LOG_STREAM(severity) Message(__func__, severity).stream() //##__VA_ARGS__
#define LAZY_STREAM(stream, condition) \
  !(condition) ? (void)0 : MessageVoidify() & (stream)
#define LOG(severity) LAZY_STREAM(LOG_STREAM(severity), ON(severity))

#define LVLOG(log, severity) !(ON(severity)) ? (void)0 : Voidify() & log
#define LVLOG_LINE(log, severity) log.set_line(); !(ON(severity)) ? (void)0 : Voidify() & log


#define GET_TYPE(type) std::string(#type)
#define GET_DEPTH(depth) [=](int i) {std::string str; str.insert(str.size(),i*2,' '); return str;}(depth)
enum class EC {
  kA,
  kB,
  kC
};
class YYY {
  int a;
  int b;
  int c;
};
class XXX {
private:
  int x;
  double y;
  std::string str;
  EC ec;
  YYY yyy;
};
int main() {
  Test_UniqueSort();
  Test_SplitString();
  Test_CustomSort();

  Test_AllOf();
  Test_AnyOf();
  Test_NoneOf();
  Test_FindIf();
  Test_ForEach();
  Test_ForEachN();

  Test_StringUtils();
  Node* x = new BNode();
  x->add(new ANode());
  x->next();
  x->next();
  x->next();
  Node::NodeIterator iter = x->begin();
  std::cout << "xxxxxx: ";
  iter->print();
  iter++;
  std::cout << "xxxxxx: ";
  iter->print();

  PrintFlag print_flag;
  std::cout << print_flag.GetFlags() << std::endl;
  print_flag.Set(PrintFlag::Flag::USE_OPT_PRINT);
  print_flag.Set(PrintFlag::Flag::LAST);
  std::cout << print_flag.IsSet(PrintFlag::Flag::USE_PARSER_PRINT) << std::endl;
  std::cout << print_flag.IsSet(PrintFlag::Flag::LAST) << std::endl;
  print_flag.UnSet(PrintFlag::Flag::LAST);
  std::cout << print_flag.GetFlags() << std::endl;
  print_flag.SetAll();
  std::cout << print_flag.GetFlags() << std::endl;

  auto expn = new Expn();
  auto base = new Base();
  std::cout << "Base val: " << base->Print() << std::endl;
  std::cout << "Expn val: " << expn->Print() << std::endl;
//  std::string SSS;
//  Message msg;
//  msg << "YY";
//  msg.print();
//  std::cout << msg.get();

//  std::ostringstream stream;
//  (true) ? (void)0 : stream << "Stream";
//  std::cout << stream.str() << std::endl;

//  (0) ? (void)0 : (MessageVoidify() & stream << "XX");
//  (false) ? stream.str("") : stream << "ZZZZ";

//  std::cout << (STRLOG() << "STRLOG").str() << std::endl;
  std::string test_str;
  STRLOG log(test_str);
  log << "XXX" << "111";
  std::cout << log.str() << std::endl;
  std::cout << test_str << std::endl;

  GSTRLOG glog;
  glog << "GLOG" << 111111 << false<<1.1 <<"END" << "\n";
  LVLOG(glog, 1) << "11111111111111111111";
  LVLOG(glog, 2) << "22222222222222222222";
  LVLOG(glog, 3) << "33333333333333333333";
  LVLOG(glog, 4) << "44444444444444444444";
  LVLOG_LINE(glog, 4) << "4444444444LINE";
  LVLOG_LINE(glog, 3) << "3333333333LINE";
  LVLOG_LINE(glog, 2) << "2222222222LINE";
  LVLOG_LINE(glog, 1) << "1111111111LINE";
  std::cout << glog.str() << std::endl;

  expn->X();
  LOG(-1) << "-------";
  LOG(0) << "00000000";
  LOG(1) << "++++++++";
  std::cout << GET_TYPE(EC::kA) << std::endl;
  std::string depth_str;
  std::cout << "bbbbb" << GET_DEPTH(5) << std::string() << "aaaaa" <<  std::endl;

  LocalBoostTest();
  TreeTest();

  std::list<int> mylist;
  std::vector<int> myvector;
  std::cout << "IsContainer Check" << std::endl;
  std::cout << is_stl_container<decltype(mylist)>::value << std::endl;
  std::cout << is_stl_container<decltype(myvector)>::value << std::endl;
  std::cout << is_stl_container<decltype(glog)>::value << std::endl;
  std::string substrtest("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
  std::cout << substrtest.substr(substrtest.find_last_of(':') + 1) << std::endl;

  std::vector<ExpOp*> exp_vector;
  exp_vector.push_back(new ExpOp());
  exp_vector.push_back(new ExpOp());
  exp_vector.push_back(new ExpOp());
  exp_vector.push_back(new ExpOp());
  PrintExp(&exp_vector);

  std::vector<ExpOp*>* op_vec_ptr;
  std::vector<ExpOp*>* op_vec_ptr2;
  op_vec_ptr2 = new std::vector<ExpOp*>();
  op_vec_ptr2->push_back(new ExpOp());
  op_vec_ptr2->push_back(new ExpOp());
  op_vec_ptr2->push_back(new ExpOp());
  op_vec_ptr2->push_back(new ExpOp());
  op_vec_ptr = op_vec_ptr2;
  std::cout << &op_vec_ptr << ", " << &op_vec_ptr2 << std::endl;
  std::cout << op_vec_ptr << ", " << op_vec_ptr2 << std::endl;
  std::cout << __LINE__ << ", size: " << op_vec_ptr->size() << std::endl;
  std::list<ExpOp*> equal_list1;
  std::list<ExpOp*> equal_list2;
  equal_list2.push_back(new ExpOp());
  equal_list2.push_back(new ExpOp());
  equal_list1 = equal_list2;
  std::cout << equal_list1.size() << std::endl;

  std::vector<ExpOp*> int_vector(3);
  std::cout << "Int vector size: " << int_vector.size() << std::endl;

  const std::string* cc = new std::string("111");
  std::string* c = new std::string("222");
  std::cout << "cc[0]: " << cc[0] << ", c[0]: " << c[0] << std::endl;
//  (*cc)[0] = (*c)[0];
//  *cc = *c;
  cc = c;
  std::cout << *cc << std::endl;
  printStaticData();
  printStaticData();
  printStaticData();
  printStaticData();
  printStaticData();
  printStaticData();

  const CTextBlock cctb("Hello");
  char *pc = &cctb[0];
//  *pc = 'J';  // Segfault나는데 어차피?
  Functor functor;
  functor.func() = 4;
  std::cout << functor.func() << std::endl;

  Initialize(3,"xxx", 5);


  NamedObject jindo(std::string("jindo"), 3);
  NamedObject corgi(std::string("corgi"), 5);
  NamedObject &dog1 = jindo;
  NamedObject &dog2 = corgi;
  dog1 = corgi;
  jindo.dog_info_print();
  dog1.dog_info_print();

  C ccc;
  B* bbb = &ccc;
  bbb->Print();

  FontHandle fh;
  Font f(fh);
  fh.Print();
//  std::cout << f.int() << std::endl;
  FontHandle fh2 = f;
  Font f2(fh);
  f2 = f;
  int iii = f;

  int* arr = new IntArray;
  arr[0] = 0;
  arr[1] = 1;
  arr[2] = 2;
  arr[3] = 3;
  arr[4] = 4;
  for (int i =0; i< 6 ;++i) std::cout << arr[i]<< ", ";
  std::cout << std::endl;

  Expr expr("aaaa",1);
  std::cout << *expr.s_ << ", bi: " << expr.bi_ << std::endl;
  Expr expr2("bbbbb",2);
  expr = expr2;
  std::cout << *expr.s_ << ", bi: " << expr.bi_ << std::endl;

  Expr expr3("ccccc", 5);
  WrapExpr wexpr(expr, 3,3);
  WrapExpr wexpr2 = wexpr;
  std::cout << wexpr2.s_->c_str() << ", " << wexpr2.i_ <<", bi: " << wexpr2.bi_ << std::endl;
  std::cout << wexpr.s_ << ", " << wexpr2.s_ << std::endl;
//  delete wexpr.s_;
  std::cout << *wexpr.s_ << ", " << *wexpr2.s_ << std::endl;
  WrapExpr wexpr3(expr3, 5,4);
  wexpr2 = wexpr3;
  std::cout << wexpr2.s_->c_str() << ", " << wexpr2.i_ <<", bi: " << wexpr2.bi_ << std::endl;

  *ui.get() = 3;
  std::cout << *ui << std::endl;
  std::unique_ptr<int> ui2 = std::move(ui);
  std::cout << ui.get() << ", " << *ui2 << std::endl;
  sp[0] = 0;
  sp[1] = 1;
  sp[2] = 2;
  std::shared_ptr<int[]> sp2 = std::move(sp);
  std::cout << sp2.use_count() << ", sp2[2]: " << sp2[2] << std::endl;
  std::cout << sp.use_count()  << std::endl;
  std::shared_ptr<int[]> sp3 = std::move(sp2);
  std::cout << sp3.use_count() << ", sp3[2]: " << sp3[2] << std::endl;
//  single[0] = 0;
//  std::shared_ptr<Expr> sharedexpr(new Expr("eee",6), std::default_delete<Expr>());
//  std::cout << sharedexpr.use_count() << std::endl;
  std::string ssa("ssa");
  std::string ssb;
  ssb = ssa.c_str();
  std::cout << ssb << std::endl;

  Expr* expr_c = new Expr("ccccc", 5);
  WrapExpr* wexpr_c = new WrapExpr(*expr_c, 3,3);
  Expr* wexpr2_c = static_cast<WrapExpr*>(expr_c);
  Expr* wexpr3_c = dynamic_cast<WrapExpr*>(expr_c);
  expr_c->print();
  wexpr_c->print();
  static_cast<Expr>(*expr_c).print();
//  static_cast<WrapExpr>(*expr_c).print();
  static_cast<WrapExpr>(*wexpr_c).print();
  static_cast<Expr>(*wexpr_c).print();
  dynamic_cast<Expr*>(expr_c)->print();
//  dynamic_cast<WrapExpr*>(expr_c)->print();
  dynamic_cast<WrapExpr*>(wexpr_c)->print();
  dynamic_cast<Expr*>(wexpr_c)->print();

  int* expr_d_int_a;
  {
    Expr* expr_d = new Expr("dddd", 5);
    int & expr_d_int_a_ref = expr_d->bi_;
    expr_d_int_a = &expr_d_int_a_ref;
    delete expr_d;
  }
  Expr* expr_aaaa = new Expr("dddd",3);
  std::cout << *expr_d_int_a << std::endl; // 결과는3. 계속 같은 Heap메모리 보고 있음. nullptr해줘야하는 이유


  //item33
  Derived33 d33;
  int x33;

  d33.mf1(); // Derived::mf1 호출 (매개변수 없는 버전)
//  d33.mf1(x33); // 에러! Base::mf1() 은 가려져 있음

  //item36
  D36 x36;

  B36 *pB36 = &x36;
  pB36->mf();

  D36 *pD36 = &x36;
  pD36->mf();
  //item37
  Shape37* rect37 = new Rectangle37();
  Shape37* circle37 = new Circle37();
  rect37->draw(); // Red호출
  circle37->draw();// Red호출
  // 해결? NVI(non-virtual interface)로 비가상 draw(color=기본값)뚫고, 내부콜로 vitual doDraw(color) 구현

  Person39 p39;
  Student39 s39;

//  eat39(p39);
  // 에러! Student 는 Person 의 일종이 아니다
  // Student 가 public 상속을 하면 정상 동작한다
//  eat39(s39);
  child_40 x40;
  std::cout << sizeof(x40) << std::endl;

  //item41
  W41 w41;
  doProcess(w41);

  //item42
//  printData(w41); //no have typename const_iterator Error.
  std::vector<int> v42{1,2,3,4,5};
  printData(v42);
  std::string s42("aaa");
  widgetType(s42.begin());

  //item44
//SquareMatrix<double, 5> sm1(1,3);
SquareMatrix<double, 5> sm1;
// SquareMatrix<double, 5>::invert 호출
sm1.invert();

//SquareMatrix<std::string, 10> sm2("aa",3);
SquareMatrix<double, 10> sm2;
// SquareMatrix<double, 10>::invert 호출
sm2.invert();

  std::cout << "----item49 test start" << std::endl;
//  std::set_new_handler(OutOfMem49);
  int * i49 = new int[2];

  Widget49::set_new_handler(OutOfMem49);
//  Widget49* w49 = new Widget49[1000000000000000000]; -> new throw(std::bad_alloc) 오버로딩 불가 -> 그냥 무조건 std::bad_alloc 뜨네...

  // 글로벌로 std::set_new_handler(OutOfMem49)가 등록되면 관련 메시지,abort출력
  // 반면 Widget49::set_new_handler만 등록되면 std::bad_alloc이 호출
//  int *pBigMemory49 = new int[10000000000000000000001];

  Widget49::set_new_handler(0);
  Widget49* w49_2 = new Widget49;
//  std::set_new_handler(0);
  int *memo49 = new int[1000];

  std::cout << "----item49 test success" << std::endl;

  return 0;
}
