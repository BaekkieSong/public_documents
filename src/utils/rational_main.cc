#include <iostream>
using std::cout;
using std::endl;

class Rational
{
  public:
    Rational(int numerator = 0, int denominator = 1);
    ~Rational();
    int numerator() const;
    int denominator() const;
//const Rational operator*(const Rational& rhs) const;
  private:
    int n;
    int d;
};

int Rational::numerator() const
{
  return n;
}

int Rational::denominator() const
{
  return d;
}

Rational::Rational(int numerator, int denominator)
{
  cout << __func__ << endl;
  n = numerator;
  d = denominator;
}
Rational::~Rational()
{
  cout << __func__ << endl;
}

// result = 2*oneEight; Error: int::operator*(const Rational&) is not defined.
//const Rational Rational::operator*(const Rational& rhs) const
//{
//  return Rational(this->n*rhs.numerator(), this->d*rhs.denominator());
//}

const Rational operator*(const Rational& lhs, const Rational& rhs)
{
  return Rational(lhs.numerator()*rhs.numerator(), rhs.denominator()*rhs.denominator());
}

void printRational(const Rational& r)
{
  cout << r.numerator() << " / " << r.denominator() << endl;
}


int main()
{
  Rational oneEight(1, 8);
  Rational oneHalf(1, 2);

  Rational result = oneHalf * oneEight;
  result = 2*oneEight;
  result = oneEight*2;
  printRational(result);

  return 0;
}
