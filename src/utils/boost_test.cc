#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <iostream>
#include <string>
#include "boost_test.h"
#include <unordered_map>
#include <vector>
#include <boost/core/demangle.hpp>
#include <typeinfo>
#include <boost/type_index.hpp>
#include <string_view>
#include <type_traits>
#include <boost/optional.hpp>

using namespace boost::property_tree;
typedef boost::property_tree::ptree Tree;
void LocalBoostTest() {
  Tree root;
  root.put("type", "node");
  Tree child1;
  child1.put("name", "User1");
  child1.put("age", 11);
  child1.add("skill", "XXX");
  child1.add("skill", "YYY");
  child1.add("skill", "ZZZ");
  root.add_child("children", child1);

  Tree child2;
  child2.put("name", "User2");
  child2.put("age", 12);
  child2.add("skill", "XXX");
  child2.add("skill", "YYY");
  child2.add("skill", "ZZZ");
  root.add_child("children", child2);

  Tree& user3 = root.add("User3", "");
  user3.put("name", "User2");
  user3.put("age", 12);
  user3.add("skill", "XXX");
  user3.add("skill", "YYY");
  user3.add("skill", "ZZZ");
  int intdata=1;
  if (std::is_fundamental<decltype(intdata)>::value)
    user3.add(std::to_string(intdata), "xxxx");


  root.push_back( std::make_pair("", child1) );
  root.push_back( std::make_pair("", child2) );

  Tree listchild1, listchild2;
  listchild1.put("",1);
  listchild2.put("",2);
  Tree child3;
  child3.push_back(std::make_pair("", listchild1));
  child3.push_back(std::make_pair("", listchild2));
  root.add_child("List", child3);



  std::string file_name("json_output");
  boost::property_tree::write_json(file_name, root);
}

namespace ExprTest {
class Expr {
  public:
    virtual void PrintName() {
      std::cout << boost::core::demangle(typeid(this).name()) << std::endl;
    }
    Expr(int c1, int c2) : common1(c1), common2(c2) {}
//  void AddPrintInfo(Tree& tree) {
//    Tree t_sub;
//    t_sub.put("common1", common1);
//    t_sub.put("common2", common2);
//    AddPrintExtentInfo(t_sub);
//    Tree t_children;
//    for (auto it : children) {
//      Tree t;
//      it->AddPrintInfo(t);
//      t_children.push_back(std::make_pair("",t));
//    }
//    t_sub.add_child("children", t_children);
//    tree.add_child("Temp", t_sub);
//  }
  void AddPrintCommonInfo(Tree& tree) {
    tree.add("common1", common1);
    tree.add("common2", common2);
    Tree t_children;
    for (auto it : children) {
//      it->GetChildPrintInfo(t_children);
      it->AddPrintExtentInfo(t_children);
    }
    tree.add_child("children", t_children);
  }
  void GetChildPrintInfo(Tree& children) {
    Tree child;
    AddPrintExtentInfo(child);
    children.push_back(std::make_pair("",child));
  }
  virtual void AddPrintExtentInfo(Tree& tree) {}
  int common1;
  int common2;
  std::vector<Expr*> children;
};
class ExprA : public Expr {
  public:
    void PrintName() override {
      std::cout << boost::core::demangle(typeid(this).name()) << std::endl;
    }
    ExprA(int c1, int c2, int a) : Expr(c1,c2), A(a){}
    virtual ~ExprA(){}
  virtual void AddPrintExtentInfo(Tree& tree) override {
    Tree& expr = tree.add("ExprA", "");
    AddPrintCommonInfo(expr);
    expr.add("A", A);
  }
    int A;
};
class ExprB : public Expr {
  public:
    void PrintName() override {
      std::cout << boost::core::scoped_demangled_name(typeid(this).name()).get() << std::endl;
    }
    ExprB(int c1, int c2, int b) : Expr(c1,c2), B(b){}
    virtual ~ExprB(){}
  virtual void AddPrintExtentInfo(Tree& tree) override {
    Tree& expr = tree.add("ExprB", "");
    AddPrintCommonInfo(expr);
    expr.add("B", B);
  }
    int B;
};
class ExprAA : public ExprA {
  public:
    ExprAA(int c1, int c2, int a) : ExprA(c1,c2,a){}
    virtual ~ExprAA(){}
};

//template<typename T>
void PrintDemangle(Expr& data) {
//  std::cout << boost::core::scoped_demangled_name(typeid(data).name()).get() << std::endl;
//  std::cout << boost::typeindex::type_id_runtime(data).pretty_name() << std::endl;
  std::string_view str = boost::core::demangle(typeid(data).name());
  //str = str.substr(str.find_last_of(':')+1, str.size() - str.find_last_of(':'));
  str = str.substr(str.find_last_of(':')+1);
  std::cout << str << std::endl;
}

void AddrTest(Expr& expr) {
  std::cout << __func__ << &expr << std::endl;
}
void AddrPtrTest(Expr* expr) {
  std::cout << __func__<<expr << std::endl;
 // std::cout << __func__<<&expr << std::endl; //diferrent
}
std::string GetClassName(Expr *obj) {
  std::string_view str = boost::core::demangle(typeid(*obj).name());
  return std::string(str.substr(str.find_last_of(':') + 1));
}

template <typename Container>
void PrintAllExprAA(Container* cont) {
  for(Expr* it : *cont) {
    PrintDemangle(*it);
  }
}

} // ExprTest

class Step {
  public:
    virtual void Print() {
      std::cout <<std::endl << std::endl<< a << std::endl;
      Print();
    }
  int a=1;
};
class Step1 : public Step {
  public:
    virtual void Print() override {
      std::cout << b << std::endl;
      Print();
    }
    int b=2;
};
class Step2 : public Step1 {
  public:
    virtual void Print() override {
      static bool count = true;
      if (count) {
        count = false;
        Step1::Print();
        std::cout << c << std::endl;
      }
      else
        count = true;
    }
    int c=3;
};
#define TO_STR(type) std::string("NOIMPL_") + std::string(type)
enum class Type {
  TypeA,
  TypeB,
  TypeC
};
std::string GetValue(Type* type) {
  std::string str = boost::core::demangle(typeid(*type).name());
  return str;
}

template<typename Container>
Tree GetContainerInfo(std::string name, Container& cont) {
  Tree tree;
  for (auto it : cont) {
    Tree t;
    t.put("", it);
    tree.push_back(std::make_pair("", t));
  }
  return tree;
}

using namespace ExprTest;
void TreeTest() {
  ExprA root(1,11,111);
  ExprB sub1(2,2,2);
  ExprB sub2(3,3,3);
  ExprA sub11(22,22,22);
  ExprB sub12(22,22,33);
  root.children.push_back(&sub1);
  root.children.push_back(&sub2);
  sub1.children.push_back(&sub11);
  sub1.children.push_back(&sub12);
  Tree root_tree;
  root.AddPrintExtentInfo(root_tree);
  Tree& child = root_tree.get_child("ExprA");
  Tree& subchild = child.get_child("children");
  for (auto it : subchild) {
    std::cout << "AAA";
  }
  Tree& subsubchild = subchild.get_child("ExprB");
  std::cout << subsubchild.get<int>("common1") << std::endl;
  Tree& subsubchild2 = subchild.get_child("ExprB");
  std::cout << subsubchild2.get<int>("common1") << std::endl;


  Step2 step;
  step.Step::Print();
  step.Step::Print();
  std::string file_name("TreeTest.log");
  boost::property_tree::write_json(file_name, root_tree);

  std::unordered_map<std::string* , std::string*> umap;
  std::string map_str("xxxx");
  std::string map_data("yyyy");
  umap[&map_str] = &map_data;
  if (auto* str = umap[&map_str]) {
    std::cout << "success" << std::endl;
    std::cout << *str << std::endl;
  } else {
    std::cout << "failed" << std::endl;
  }
  Type type = Type::TypeA;
  std::cout << TO_STR("DataAAAAA") << std::endl;
  std::cout << "TypeValue: " << GetValue(&type) << std::endl;

  Tree test_root;
  std::vector<int> datas;
  datas.push_back(1);
  datas.push_back(2);
  datas.push_back(3);
  datas.push_back(4);
  datas.push_back(5);
  std::string dataname="datas";
  Tree test_tree = GetContainerInfo<std::vector<int>>(dataname, datas);
  test_root.add_child("datas", test_tree);
  std::string file_name2("container");
  boost::property_tree::write_json(file_name2, test_root);
  PrintDemangle(root);
  PrintDemangle(sub1);
  PrintDemangle(sub2);
  PrintDemangle(sub11);
  PrintDemangle(sub12);
//  std::cout << is_member_object_pointer<root>::value;
//  std::cout << is_member_object_pointer<*root>::value;
  Expr* obj = &root;
  std::cout << std::is_pointer<decltype(obj)>::value;
  std::cout << std::is_pointer<decltype(root)>::value << std::endl;
  std::cout << std::is_base_of_v<Expr, Expr> << std::endl;
  std::cout << std::is_base_of_v<Expr, ExprA> << std::endl;
  std::cout << std::is_base_of_v<Expr, ExprB> << std::endl;
  std::cout << std::is_base_of_v<Expr, ptree> << std::endl;

  ExprA * addr1 = new ExprA(1,1,1);
  Expr * addr2 = new ExprA(1,1,1);
  ExprB * addr3 = new ExprB(1,1,1);
  ExprA addr4 = ExprA(1,1,1);
  std::cout << addr1 << std::endl;
  std::cout << addr2 << std::endl;
  std::cout << addr3 << std::endl;
  std::cout << &addr4 << std::endl;
  AddrTest(*addr1);
  AddrTest(*addr2);
  AddrTest(*addr3);
  AddrTest(addr4);
  AddrPtrTest(addr1);
  AddrPtrTest(addr2);
  AddrPtrTest(addr3);
  AddrPtrTest(&addr4);
//  std::vector<Expr*> expr_vec;
//  expr_vec.push_back(addr1);
//  std::cout << "value_type: "<< sizeof(decltype(expr_vec)::value_type) << std::endl;
//  //std::cout << "GetClassName : " << GetClassName(*expr_vec.end()) << std::endl;
//  std::vector<int>* int_vec;// = new std::vector<int>;
//  //int_vec->push_back(addr1);
//  std::cout << "int_vec" << std::is_null_pointer<decltype(int_vec)>::value << std::endl;
//  std::cout << int_vec->empty() << std::endl;
//  std::cout << int_vec->size() << std::endl;
//  std::cout << int_vec->max_size() << std::endl;
//  for (auto it : *int_vec) {
//    std::cout << 1;
//    std::cout << it;
//  }
//
//  std::vector<Expr*> expr_vec2;
//  std::cout << "expr_vec2"<<std::is_null_pointer<decltype(expr_vec2)>::value << std::endl;
//  std::cout << expr_vec2.empty() << std::endl;
//  std::cout << expr_vec2.size() << std::endl;
//  ExprA* testnull;
//  std::cout << "testnull"<<std::is_null_pointer<decltype(testnull)>::value << std::endl;
//  std::cout << testnull << std::endl;
//
//  if (int_vec)
//    std::cout << "Good?" << std::endl;
//  std::cout << (int_vec) << std::endl;

  std::vector<ExprAA*>  aa_list;
  aa_list.push_back(new ExprAA(1,1,1));
  aa_list.push_back(new ExprAA(1,1,1));
  aa_list.push_back(new ExprAA(1,1,1));
  aa_list.push_back(new ExprAA(1,1,1));
  aa_list.push_back(new ExprAA(1,1,1));
  PrintAllExprAA(&aa_list);


}
