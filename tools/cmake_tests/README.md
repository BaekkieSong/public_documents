# cmake 테스트 폴더
* CMakeLists.txt 기본 템플릿 작성, 빌드, 설치 테스트
## 주요 기능
* static, shared 라이브러리 빌드 및 API 호출
* install 기능 런타임 링킹 테스트
* custom command/target 기능 테스트
## 테스트 빌드
* `cmake -H. -Bbuild -DCMAKE_INSTALL_PREFIX=./build/install_dir`
* `cmake --build build --target install`
  * 또는 `cd build && make install`
## 테스트 실행
* `./build/install_dir/bin/project_exe`
## 테스트 파일 제거(Unstaged, install 파일)
* `cmake --build build --target clean-test`
## Tip
* shared library 인식을 못 할 경우
  * `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:[테스트.so절대경로]`
* install 기능 관련
  * install 기능으로 복사할 header 파일 설정 방법
    * SET_TARGET_PROPERTIES(mylib PROPERTIES PUBLIC_HEADER "some.h;another.h")
  * CMAKE_INSTALL_PREFIX를 CMakeLists.txt에 설정하는 방법
    * SET(CMAKE_INSTALL_PREFIX < install_path >)
    * 주의 - 반드시 PROJECT(< project_name >) 앞에 설정
  * CMAKE_INSTALL_RPATH를 CMakeLists.txt에 설정하는 방법
    * SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
    * SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
    * 주의 - ADD_EXECUTABLE(< exe_name >) 앞에 설정
---
### Reference
* [INSTALL PUBLIC HEADER 설정 방법](https://stackoverflow.com/questions/10487256/cmake-how-to-properly-copy-static-librarys-header-file-into-usr-include)
* [INSTALL PREFIX 설정 방법](https://stackoverflow.com/questions/6241922/how-to-use-cmake-install-prefix)
* [RPATH 설정 방법](https://stackoverflow.com/questions/32469953/why-is-cmake-designed-so-that-it-removes-runtime-path-when-installing/32470070#32470070)
