function(set_target_compile_options_for_platform TARGET_NAME)
  if(MSVC)
    target_compile_options(${TARGET_NAME}
    PUBLIC
      /std:c++latest /W4  # MSVC 가 식별 가능한 옵션을 지정
      )

    # 묵시적으로 #define을 추가 (컴파일 시점에 적용)
    target_compile_definitions(${TARGET_NAME}
    PRIVATE
      WIN32_LEAN_AND_MEAN
      NOMINMAX    # numeric_limits를 사용할때 방해가 되는
                  # max(), min() Macro를 제거
      _CRT_SECURE_NO_WARNINGS
      )

  else() # Clang + GCC
    target_compile_options(${TARGET_NAME}
    PUBLIC
      -std=c++17 -g -Wall    # GCC/Clang이 식별 가능한 옵션을 지정
    PRIVATE
      -fPIC
      -fno-rtti
      )

  endif()
endfunction()

#ADD_COMPILE_OPTIONS (
#    -std=c++14
#    -g    #Debug 심볼 테이블 포함
#    -Wall #모든 경로메시지 포함
#    )
#SET_TARGET_PROPERTIES (${TARGET_NAME} PROPERTIES COMPILE_FLAGS "-std=c++14 -g")
